/* istanbul ignore file */

'use strict';

const {
    CardReferentialDataDownloaderService,
} = require('../services/import_data/CardReferentialDataDownloaderService');

const data_source_base_path = `${__dirname}/../../../data/source/`;

const card_referential_data_downloader_service = CardReferentialDataDownloaderService.getInstance();

(async () => {
    card_referential_data_downloader_service.setDataFilePath(data_source_base_path);
    await card_referential_data_downloader_service.processDownloadReferentials();
})();

