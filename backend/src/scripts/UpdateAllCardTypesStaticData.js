/* istanbul ignore file */

/**
 * Script to preview (without saving) all type_1 in
 * precalculated cards values
 */


'use strict';

const {
    CardRepository,
} = require('../repositories');
const {
    MongoDbRepository,
} = require('../repositories/MongoDbRepository');
const {
    JsonFileRepository,
} = require('../repositories');

const data_source_base_path = `${__dirname}/../static_data/`;
const referential_file_name = 'card-type-referential.json';

const json_file_repository = JsonFileRepository.getInstance();
const card_repository = CardRepository.getInstance();
const mongo_db_repository = MongoDbRepository.getInstance();


(async () => {

    let saved_data = [];
    try {
        saved_data = await json_file_repository.getData(`${data_source_base_path}${referential_file_name}`);
    } catch (error) {
        if (error.message && error.message.includes('JSON')) {
            console.log(error.message);
            console.log('Broken JSON referential file');
            // eslint-disable-next-line no-process-exit
            process.exit(0);
        }
        console.log('Was not able to get data');
    }

    const type_list = [];

    const card_list = await card_repository.search({
        field_list: [
            'precalculated_value',
        ],
    });

    console.log(card_list.length);

    card_list.forEach((card) => {

        if (!type_list.includes(card.precalculated_value.type_1)
            && card.precalculated_value.type_1 !== null
        ) {
            type_list.push(card.precalculated_value.type_1);
        }

        if (!type_list.includes(card.precalculated_value.type_2)
            && card.precalculated_value.type_2 !== null
        ) {
            type_list.push(card.precalculated_value.type_2);
        }

    });

    console.log(type_list.length);

    const types = type_list.map((type) => {
        const existing_item = saved_data.find((item) => item.value === type);

        if (existing_item === undefined) {
            return {
                value: type,
                text: '',
            };
        }
        return existing_item;
    });

    const stringifyed_referential = JSON.stringify(types, null, 4);
    await json_file_repository.saveFile(
        `${data_source_base_path}${referential_file_name}`,
        stringifyed_referential
    );
    mongo_db_repository.closeConnection();
})();
