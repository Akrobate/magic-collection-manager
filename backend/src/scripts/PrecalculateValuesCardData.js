/* istanbul ignore file */

'use strict';

const {
    MongoDbRepository,
} = require('../repositories');

const {
    CardValuesPrecalculatorService,
} = require('../services');

(async () => {
    await CardValuesPrecalculatorService.getInstance().processCardValuesPrecalculate();
    await MongoDbRepository.getInstance().closeConnection();
})();


// [
//     'R',       'W',       'U',
//     'G',       'G_W',     'G_U',
//     'R_W',     'B',       'B_R',
//     '',        'B_G_R',   'U_W',
//     'B_U_W',   'B_U',     'R_U',
//     'R_U_W',   'B_W',     'G_U_W',
//     'G_R',     'B_G_W',   'B_R_U',
//     'B_G',     'B_R_W',   'B_G_R_U_W',
//     'G_R_U',   'B_G_U',   'G_R_W',
//     'B_R_U_W', 'B_G_U_W', 'B_G_R_U',
//     'B_G_R_W', 'G_R_U_W'
//   ]
