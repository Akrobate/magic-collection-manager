/* istanbul ignore file */

'use strict';

const {
    CardDataDownloaderService,
} = require('../services/import_data/CardDataDownloaderService');

const data_source_base_path = `${__dirname}/../../../data/source/`;

const card_data_downloader_service = CardDataDownloaderService.getInstance();

(async () => {
    card_data_downloader_service.setDataFilePath(data_source_base_path);
    await card_data_downloader_service.processCardDataDownload();
})();
