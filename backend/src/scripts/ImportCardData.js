/* istanbul ignore file */

'use strict';

const {
    MongoDbRepository,
} = require('../repositories');

const {
    ImportDataService,
} = require('../services');

const mongo_db_repository = MongoDbRepository.getInstance();
const import_data_service = ImportDataService.getInstance();

const data_source_base_path = `${__dirname}/../../../data/source/`;

let file_name = 'all-cards-20210831091120.json';

import_data_service.setDataFolderPath(data_source_base_path);
file_name = import_data_service.getLastDownloadedAllCardFileName();
console.log(`Importing file: ${file_name}`);

const file_path = `${data_source_base_path}${file_name}`;

(async () => {
    import_data_service.setDataFilePath(file_path);
    await mongo_db_repository.createIndex('cards', {
        id: 1,
    });
    await import_data_service.processForceUpdateCreateItems();

    await mongo_db_repository.closeConnection();
})();
