'use strict';

/**
 * Draft script
 *
 * Used to init in "production" mode my own decks that
 * are used to seed files
 *
 * Once finished this script will be removed
 */


const {
    DataSeeder,
} = require('../../test/test_helpers/DataSeeder');

const deck_green = require('../../test/test_seeds/decks/deck_green');

console.log(deck_green);

(async () => {
    await DataSeeder.loadDecksSeedInCardListItemRepository(deck_green, 1);
    console.log('Finished');
})();

