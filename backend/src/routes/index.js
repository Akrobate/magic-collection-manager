'use strict';

const {
    Router,
} = require('express');

const {
    CardController,
    UserController,
    CardListController,
    CollectionUserCardController,
    AggregatedSearchCardController,
    CardReferentialController,
} = require('../controllers');

const {
    AuthenticationMiddleware,
} = require('../middlewares/AuthenticationMiddleware');

const api_routes = Router(); // eslint-disable-line new-cap

const authentication_middleware = AuthenticationMiddleware.getInstance();
const user_controller = UserController.getInstance();
const card_controller = CardController.getInstance();
const collection_user_card_controller = CollectionUserCardController.getInstance();
const aggregated_search_card_controller = AggregatedSearchCardController.getInstance();
const card_list_controller = CardListController.getInstance();
const card_referential_controller = CardReferentialController.getInstance();

const url_prefix = '/api/v1';


// Cards
api_routes.get(
    '/cards',
    (request, response, next) => card_controller
        .search(request, response)
        .catch(next)
);

api_routes.get(
    '/cards/count',
    (request, response, next) => card_controller
        .count(request, response)
        .catch(next)
);

// Card referentials
api_routes.get(
    '/cards/referentials/colors',
    (request, response, next) => card_referential_controller
        .getCardReferentialColor(request, response)
        .catch(next)
);

api_routes.get(
    '/cards/referentials/sets',
    (request, response, next) => card_referential_controller
        .getCardReferentialSet(request, response)
        .catch(next)
);

api_routes.get(
    '/cards/referentials/types',
    (request, response, next) => card_referential_controller
        .getCardReferentialType(request, response)
        .catch(next)
);

api_routes.get(
    '/cards/referentials/meta-types',
    (request, response, next) => card_referential_controller
        .getCardReferentialMetaType(request, response)
        .catch(next)
);

// Collection user cards
api_routes.get(
    '/users/:user_id/collection-user-cards',
    authentication_middleware.injectJwtData(),
    (request, response, next) => collection_user_card_controller
        .search(request, response)
        .catch(next)
);

api_routes.post(
    '/users/:user_id/collection-user-cards',
    authentication_middleware.injectJwtData(),
    (request, response, next) => collection_user_card_controller
        .create(request, response)
        .catch(next)
);

api_routes.delete(
    '/users/:user_id/collection-user-cards/:id',
    authentication_middleware.injectJwtData(),
    (request, response, next) => collection_user_card_controller
        .delete(request, response)
        .catch(next)
);

// Card list
api_routes.get(
    '/users/:user_id/card-lists',
    authentication_middleware.injectJwtData(),
    (request, response, next) => card_list_controller
        .search(request, response)
        .catch(next)
);

api_routes.post(
    '/users/:user_id/card-lists',
    authentication_middleware.injectJwtData(),
    (request, response, next) => card_list_controller
        .create(request, response)
        .catch(next)
);

api_routes.patch(
    '/users/:user_id/card-lists/:id',
    authentication_middleware.injectJwtData(),
    (request, response, next) => card_list_controller
        .update(request, response)
        .catch(next)
);

api_routes.delete(
    '/users/:user_id/card-lists/:id',
    authentication_middleware.injectJwtData(),
    (request, response, next) => card_list_controller
        .delete(request, response)
        .catch(next)
);

// Duplicate
api_routes.post(
    '/users/:user_id/card-lists/:id/duplicate',
    authentication_middleware.injectJwtData(),
    (request, response, next) => card_list_controller
        .duplicate(request, response)
        .catch(next)
);


// CardListItems search
api_routes.get(
    '/users/:user_id/card-list-items',
    authentication_middleware.injectJwtData(),
    (request, response, next) => card_list_controller
        .searchCardListItem(request, response)
        .catch(next)
);

// CardList Items
api_routes.get(
    '/users/:user_id/card-lists/:card_list_id/items',
    authentication_middleware.injectJwtData(),
    (request, response, next) => card_list_controller
        .searchItem(request, response)
        .catch(next)
);

api_routes.post(
    '/users/:user_id/card-lists/:card_list_id/items',
    authentication_middleware.injectJwtData(),
    (request, response, next) => card_list_controller
        .createItem(request, response)
        .catch(next)
);

api_routes.delete(
    '/users/:user_id/card-lists/:card_list_id/items/:card_list_item_id',
    authentication_middleware.injectJwtData(),
    (request, response, next) => card_list_controller
        .deleteItem(request, response)
        .catch(next)
);


// Aggregated searches
api_routes.get(
    '/users/:user_id/aggregated/collection',
    authentication_middleware.injectJwtData(),
    (request, response, next) => aggregated_search_card_controller
        .searchInCollection(request, response)
        .catch(next)
);

api_routes.get(
    '/users/:user_id/aggregated',
    authentication_middleware.injectJwtData(),
    (request, response, next) => aggregated_search_card_controller
        .searchInCardList(request, response)
        .catch(next)
);

api_routes.get(
    '/users/:user_id/aggregated/count',
    authentication_middleware.injectJwtData(),
    (request, response, next) => aggregated_search_card_controller
        .countInCardList(request, response)
        .catch(next)
);


// Users
api_routes.post(
    '/users/register',
    (request, response, next) => user_controller
        .register(request, response)
        .catch(next)
);

api_routes.post(
    '/users/login',
    (request, response, next) => user_controller
        .login(request, response)
        .catch(next)
);

api_routes.post(
    '/users/token/renew',
    authentication_middleware.injectJwtData(),
    (request, response, next) => user_controller
        .renewToken(request, response)
        .catch(next)
);

api_routes.patch(
    '/users/:user_id',
    authentication_middleware.injectJwtData(),
    (request, response, next) => user_controller
        .update(request, response)
        .catch(next)
);

api_routes.get(
    '/users/:user_id',
    authentication_middleware.injectJwtData(),
    (request, response, next) => user_controller
        .read(request, response)
        .catch(next)
);

module.exports = {
    api_routes,
    url_prefix,
};
