'use strict';

const {
    DataTypes,
    Op,
} = require('sequelize');

const {
    sequelize,
} = require('./connections/Sequelize');

const {
    AbstractSequelizeRepository,
} = require('./AbstractSequelizeRepository');

class CardListItemRepository extends AbstractSequelizeRepository {

    /**
     * @static
     * @returns {CardListItemRepository}
     */
    static getInstance() {
        if (CardListItemRepository.instance === null) {
            CardListItemRepository.instance = new CardListItemRepository(
                CardListItemRepository.getModel()
            );
        }

        return CardListItemRepository.instance;
    }


    /**
     * @returns {Sequelize<Object>}
     */
    static getModel() {
        return sequelize.define(
            'CardListItem',
            {
                id: {
                    allowNull: false,
                    autoIncrement: true,
                    primaryKey: true,
                    type: DataTypes.INTEGER.UNSIGNED,
                },
                card_list_id: {
                    allowNull: false,
                    type: DataTypes.INTEGER.UNSIGNED,
                    unique: false,
                },
                card_id: {
                    allowNull: false,
                    type: DataTypes.STRING,
                    unique: false,
                },
                oracle_id: {
                    allowNull: false,
                    type: DataTypes.STRING,
                    unique: false,
                },
                state: {
                    allowNull: false,
                    type: DataTypes.INTEGER.UNSIGNED,
                    unique: false,
                },
                created_at: {
                    allowNull: false,
                    type: DataTypes.DATE,
                },
                updated_at: {
                    allowNull: false,
                    type: DataTypes.DATE,
                },
            },
            {
                tableName: 'card_list_items',
                underscored: true,
                timestamps: false,
            }
        );
    }

    /**
     * @static
     * @param {Object} criteria
     * @returns {Object}
     */
    static _formatCriteria(criteria) {
        const {
            card_id,
            card_id_list,
            card_list_id,
            card_list_id_list,
            group_by_card_id,
            group_by_oracle_id,
            sort_by_count,
            count_lower_boundary,
            count_upper_boundary,
            count,
        } = criteria;

        const where = {
            [Op.and]: {},
        };

        if (card_id !== undefined) {
            where[Op.and].card_id = {
                [Op.eq]: card_id,
            };
        }

        if (card_id_list !== undefined) {
            where[Op.and].card_id = {
                [Op.in]: card_id_list,
            };
        }

        if (card_list_id !== undefined) {
            where[Op.and].card_list_id = {
                [Op.eq]: card_list_id,
            };
        }

        if (card_list_id_list !== undefined) {
            where[Op.and].card_list_id = {
                [Op.in]: card_list_id_list,
            };
        }

        let group = null;
        let attributes = null;

        if (group_by_card_id) {
            group = ['card_id'];
            attributes = [
                'card_id',
                [sequelize.fn('ANY_VALUE', sequelize.col('oracle_id')), 'oracle_id'],
                [sequelize.fn('COUNT', sequelize.col('card_id')), 'count'],
            ];
        }

        if (group_by_oracle_id) {
            group = ['oracle_id'];
            attributes = [
                [sequelize.fn('ANY_VALUE', sequelize.col('card_id')), 'card_id'],
                'oracle_id',
                [sequelize.fn('COUNT', sequelize.col('card_id')), 'count'],
            ];
        }

        let order = null;
        if (sort_by_count === 'asc') {
            order = [[sequelize.literal('count'), 'ASC']];
        }

        if (sort_by_count === 'desc') {
            order = [[sequelize.literal('count'), 'DESC']];
        }

        const having = {
            [Op.and]: [],
        };

        if (count_lower_boundary) {
            having[Op.and].push({
                count: {
                    [Op.gte]: count_lower_boundary,
                },
            });
        }

        if (count_upper_boundary) {
            having[Op.and].push({
                count: {
                    [Op.lte]: count_upper_boundary,
                },
            });
        }

        if (count) {
            having[Op.and].push({
                count: {
                    [Op.eq]: count,
                },
            });
        }

        return {
            where,
            group,
            order,
            attributes,
            having,
        };
    }
}

CardListItemRepository.instance = null;

module.exports = {
    CardListItemRepository,
};
