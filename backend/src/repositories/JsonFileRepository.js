'use strict';

const fs = require('fs').promises;

class JsonFileRepository {

    /**
     * @static
     * @returns {JsonFileRepository}
     */
    static getInstance() {
        if (JsonFileRepository.instance) {
            return JsonFileRepository.instance;
        }
        JsonFileRepository.instance = new JsonFileRepository();
        return JsonFileRepository.instance;
    }


    /**
     * @param {String} file_path
     * @return {Promise<Object>}
     */
    async getData(file_path) {
        const string_data = await fs.readFile(
            `${file_path}`,
            'utf8'
        );
        return JSON.parse(string_data);
    }


    /**
     * @param {String} file_path
     * @param {Object} data
     * @return {Promise<Object>}
     */
    saveData(file_path, data) {
        const string_data = JSON.stringify(data);
        return this.saveFile(file_path, string_data);
    }


    /**
     * @param {String} file_path
     * @param {String} string_data
     * @return {Promise<Object>}
     */
    saveFile(file_path, string_data) {
        return fs.writeFile(
            `${file_path}`,
            string_data
        );
    }
}

module.exports = {
    JsonFileRepository,
};
