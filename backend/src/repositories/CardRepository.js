'use strict';

const {
    CustomError,
} = require('../CustomError');

const {
    MongoDbRepository,
} = require('../repositories/MongoDbRepository');

class CardRepository {

    // eslint-disable-next-line require-jsdoc
    constructor(mongo_db_repository) {
        this.mongo_db_repository = mongo_db_repository;

        this.collection_name = 'cards';
    }

    /* istanbul ignore next */
    /**
     * @static
     * @returns {CardRepository}
     */
    static getInstance() {
        if (CardRepository.instance === null) {
            CardRepository.instance = new CardRepository(
                MongoDbRepository.getInstance()
            );
        }
        return CardRepository.instance;
    }


    /**
     * @async
     * @param {Object} input
     * @returns {Promise<Object>}
     */
    async create(input) {
        const result = await this.mongo_db_repository.insertDocument(this.collection_name, input);
        const {
            insertedId,
        } = result;
        return {
            ...input,
            _id: insertedId,
        };
    }


    /**
     * @async
     * @param {Object} input
     * @returns {Promise<Object>}
     */
    createList(input) {
        return this.mongo_db_repository
            .insertDocumentList(this.collection_name, input);
    }


    /**
     * @async
     * @param {String} id
     * @param {Object} input
     * @returns {Promise<Object>}
     */
    async update(id, input) {
        await this.mongo_db_repository.updateDocument(
            this.collection_name,
            {
                _id: this.mongo_db_repository.builObjectIdFromString(id),
            },
            input
        );

        return this.read(id);
    }

    /**
     * @async
     * @param {String} id
     * @param {Object} input
     * @returns {Promise<Object>}
     */
    async replace(id, input) {
        await this.mongo_db_repository.replaceDocument(
            this.collection_name,
            {
                _id: this.mongo_db_repository.builObjectIdFromString(id),
            },
            input
        );

        return this.read(id);
    }


    /**
     * @async
     * @param {String} id
     * @returns {Promise<Object>}
     */
    async read(id) {
        const result = await this.mongo_db_repository.findDocument(
            this.collection_name,
            {
                _id: this.mongo_db_repository.builObjectIdFromString(id),
            }
        );
        if (result === null) {
            throw new CustomError(CustomError.NOT_FOUND, `Card "${id}" does not exists`);
        }
        return result;
    }


    /**
     * @return {Array}
     */
    searchCriteriaParams() {
        return {
            _id_list: {
                custom: true,
                operator: null,
                field: null,
            },
            quick_search: {
                custom: true,
                operator: null,
                field: null,
            },
            type_list: {
                custom: true,
                operator: null,
                field: null,
            },
            sub_type_list: {
                custom: true,
                operator: null,
                field: null,
            },
            id: {
                custom: false,
                operator: '$eq',
                field: 'id',
            },
            id_list: {
                custom: false,
                operator: '$in',
                field: 'id',
            },
            lang: {
                custom: false,
                operator: '$eq',
                field: 'lang',
            },
            lang_list: {
                custom: false,
                operator: '$in',
                field: 'lang',
            },
            power: {
                custom: false,
                operator: '$eq',
                field: 'power',
            },
            mana_cost: {
                custom: false,
                operator: '$eq',
                field: 'cmc',
            },
            toughness: {
                custom: false,
                operator: '$eq',
                field: 'toughness',
            },
            collector_number: {
                custom: false,
                operator: '$eq',
                field: 'collector_number',
            },
            collector_number_list: {
                custom: false,
                operator: '$in',
                field: 'collector_number',
            },
            set: {
                custom: false,
                operator: '$eq',
                field: 'set',
            },
            set_list: {
                custom: false,
                operator: '$in',
                field: 'set',
            },
            oracle_id: {
                custom: false,
                operator: '$eq',
                field: 'oracle_id',
            },
            oracle_id_list: {
                custom: false,
                operator: '$in',
                field: 'oracle_id',
            },
            color_identity_list: {
                custom: true,
                operator: '$all',
                field: 'color_identity',
            },
            keyword_list: {
                custom: false,
                operator: '$all',
                field: 'keywords',
            },
        };
    }

    /**
     * @param {Object} input
     * @returns {Bool}
     */
    checkIfInputHasFilteringCriteria(input) {
        const all_criterias = this.searchCriteriaParams();

        const filtering_criteria = Object.keys(input)
            .filter((criteria_key) => all_criterias[criteria_key] !== undefined)
            .length;
        return filtering_criteria > 0;
    }


    /**
     * // https://docs.mongodb.com/manual/reference/operator/aggregation/reduce/
     * @param {Object} input
     * @returns {Promise<Array>}
     */
    formatSearchCriteria(input) {
        const {
            limit,
            offset,
        } = input;

        const filter = {
            $and: {},
        };

        const all_criterias = this.searchCriteriaParams();

        for (const criteria_key of Object.keys(input)) {
            if (all_criterias[criteria_key] !== undefined
                && all_criterias[criteria_key].custom === false
            ) {
                if (input[criteria_key]) {
                    filter.$and[all_criterias[criteria_key].field] = {
                        [all_criterias[criteria_key].operator]: input[criteria_key],
                    };
                }
            }
        }

        if (input._id_list) {
            filter.$and._id = {
                $in: input.id_list
                    .map((id) => this.mongo_db_repository.builObjectIdFromString(id)),
            };
        }

        if (input.color_identity_list) {
            if (input.color_identity_list.includes('C')) {
                filter.$and.color_identity = [];
            } else {
                filter.$and.color_identity = {
                    $all: input.color_identity_list,
                };
            }
        }

        if (input.type_list) {
            filter.$and['precalculated_value.type_1'] = {
                $in: input.type_list,
            };
        }

        if (input.sub_type_list) {
            filter.$and['precalculated_value.sub_type_1'] = {
                $in: input.sub_type_list,
            };
        }

        if (input.quick_search) {
            filter.$and.$or = [
                {
                    name: {
                        $regex: input.quick_search,
                        $options: 'i',
                    },
                },
                {
                    printed_name: {
                        $regex: input.quick_search,
                        $options: 'i',
                    },
                },
                {
                    card_faces: {
                        $elemMatch: {
                            $or: [
                                {
                                    name: {
                                        $regex: input.quick_search,
                                        $options: 'i',
                                    },
                                },
                                {
                                    printed_name: {
                                        $regex: input.quick_search,
                                        $options: 'i',
                                    },
                                },
                            ],
                        },
                    },
                },
            ];
        }

        const field_projection = {};
        if (input.field_list) {
            input.field_list.forEach((field) => {
                field_projection[field] = true;
            });
        }

        let sort = null;

        if (input.sort) {
            sort = this._formatSort([
                input.sort,
            ]);
        }

        if (input.sort_list) {
            sort = this._formatSort(input.sort_list);
        }

        return {
            field_projection,
            limit,
            offset,
            filter,
            sort,
        };
    }


    /**
     * @async
     * @param {Object} input
     * @returns {Promise<Array>}
     */
    search(input) {

        const {
            filter,
            limit,
            offset,
            sort,
            field_projection,
        } = this.formatSearchCriteria(input);

        return this.mongo_db_repository.findDocumentList(
            this.collection_name,
            filter,
            limit,
            offset,
            sort,
            field_projection
        );
    }


    /**
     * @async
     * @param {Object} input
     * @returns {Promise<Number>}
     */
    count(input) {

        const {
            filter,
            limit,
            offset,
        } = this.formatSearchCriteria(input);

        return this.mongo_db_repository
            .countDocumentList(this.collection_name, filter, limit, offset);
    }


    /**
     * @param {String} id
     * @returns {Promise<*>}
     */
    async delete(id) {
        const result = await this.mongo_db_repository.deleteDocument(
            this.collection_name,
            {
                _id: this.mongo_db_repository.builObjectIdFromString(id),
            }
        );
        if (result.deletedCount === 0) {
            throw new CustomError(CustomError.NOT_FOUND);
        }
        return result;
    }


    /**
     * @param {Array} sort_list
     * @returns {Object}
     */
    _formatSort(sort_list) {

        const sort_object = {};

        sort_list.forEach((sort_string) => {
            if (sort_string.startsWith('-') === true) {
                sort_object[sort_string.substring(1)] = -1;
            } else {
                sort_object[sort_string] = 1;
            }
        });
        return sort_object;
    }


    /**
     * @param {String} line
     * @returns {Promies<String>}
     */
    async getAllIdList() {
        const existing_id_result = await this.search({
            field_list: ['id'],
        });
        return existing_id_result.map((item) => item.id);
    }
}

CardRepository.instance = null;

module.exports = {
    CardRepository,
};
