'use strict';

const {
    ScryfallRepository,
} = require('./ScryfallRepository');


module.exports = {
    ScryfallRepository,
};
