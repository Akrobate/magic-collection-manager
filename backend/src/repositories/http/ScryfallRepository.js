'use strict';

const axios = require('axios');

class ScryfallRepository {

    // eslint-disable-next-line require-jsdoc
    constructor() {
        this.base_api_url = 'https://api.scryfall.com';
    }

    /* istanbul ignore next */
    /**
     * @returns {ScryfallRepository}
     */
    static getInstance() {
        if (ScryfallRepository.instance === null) {
            ScryfallRepository.instance = new ScryfallRepository();
        }
        return ScryfallRepository.instance;
    }


    /**
     * @returns {Promise<Array>}
     */
    async getCardSymbols() {
        const result = await axios.get(`${this.base_api_url}/symbology`);
        return result.data;
    }

    /**
     * @returns {Promise<Array>}
     */
    async getBulkData() {
        const result = await axios.get(`${this.base_api_url}/bulk-data`);
        return result.data;
    }

    /**
     * @returns {Promise<Array>}
     */
    async getCardSets() {
        const result = await axios.get(`${this.base_api_url}/sets`);
        return result.data;
    }

}

ScryfallRepository.instance = null;


module.exports = {
    ScryfallRepository,
};

