'use strict';

const {
    MongoClient,
    ObjectId,
} = require('mongodb');

const {
    configuration,
} = require('../configuration/configuration');

const {
    logger,
} = require('../logger');

class MongoDbRepository {

    /**
     * connection pattern
     * 'mongodb://'+DATABASEUSERNAME+':'+DATABASEPASSWORD+'@'+DATABASEHOST+':'DATABASEPORT+'/
     *
     * @static
     * @returns {String}
     */
    static get CONNECTION_CREDENTIAL() {

        const {
            username,
            password,
            host,
            port,
            database_name,
        } = configuration.storage.mongodb;

        const DATABASE_HOST = host;
        const DATABASE_PORT = port;
        const DATABASE_USERNAME = username;
        const DATABASE_PASSWORD = password;

        return `mongodb://${DATABASE_USERNAME}:${DATABASE_PASSWORD}@${DATABASE_HOST}:${DATABASE_PORT}/${database_name}?authSource=admin`;
    }


    /**
     * @returns {String}
     */
    static get DATABASE_NAME() {
        const {
            database_name,
        } = configuration.storage.mongodb;
        return database_name;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {MongoDbRepository}
     */
    static getInstance() {
        if (MongoDbRepository.instance === null) {
            MongoDbRepository.instance = new MongoDbRepository(
                MongoClient
            );
        }
        return MongoDbRepository.instance;
    }


    /**
     * @param {MongoClient} mongodb_client
     */
    constructor(
        mongodb_client
    ) {
        this.mongodb_client = mongodb_client;
        this.mongodb_connection_handler = null;
        this.mongodb_connection_database_handler = {};
    }


    /**
     * @async
     * @returns {Promise<Object|error>}
     */
    async getConnection() {
        if (this.mongodb_connection_handler === null) {
            const connection = await MongoClient.connect(MongoDbRepository.CONNECTION_CREDENTIAL, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            });
            this.mongodb_connection_handler = connection;
            logger.info('Connection to mongodb: Success');
        }
        return this.mongodb_connection_handler;
    }


    /**
     * Close the connection to mongodb
     * Must be called, if not script will not exit
     * @async
     * @returns {Promse<Object|error>}
     */
    async closeConnection() {
        const result = await this.mongodb_connection_handler.close();
        this.cleanConnectionDatabaseHandler();
        this.mongodb_connection_handler = null;
        return result;
    }


    /**
     * Selects the database to use
     *
     * @param {String} database_name
     * @returns {Promise<Object>}
     */
    useDataBase(database_name) {
        if (this.mongodb_connection_database_handler[database_name] === undefined) {
            this.mongodb_connection_database_handler[database_name] = this
                .mongodb_connection_handler
                .db(database_name);
        }
        return this.mongodb_connection_database_handler[database_name];
    }


    /**
     * @returns {void}
     */
    cleanConnectionDatabaseHandler() {
        Object.keys(this.mongodb_connection_database_handler).forEach((key) => {
            delete this.mongodb_connection_database_handler[key];
        });
    }


    /**
     * @returns {ObjectId}
     */
    buildNewObjectId() {
        return new ObjectId();
    }


    /**
     * @param {String} object_id_string
     * @returns {ObjectId}
     */
    builObjectIdFromString(object_id_string) {
        return new ObjectId(object_id_string);
    }


    /**
     * Insert single document
     * @async
     * @param {String} collection_name
     * @param {String} document
     * @returns {Promise<Object|error>}
     */
    async insertDocument(collection_name, document) {
        await this.getConnection();
        const dbo = await this.useDataBase(MongoDbRepository.DATABASE_NAME);
        return dbo.collection(collection_name).insertOne(document);
    }

    /**
     * Insert single document
     * @async
     * @param {String} collection_name
     * @param {String} document_list
     * @returns {Promise<Object|error>}
     */
    async insertDocumentList(collection_name, document_list) {
        await this.getConnection();
        const dbo = await this.useDataBase(MongoDbRepository.DATABASE_NAME);
        return dbo.collection(collection_name).insertMany(document_list);
    }


    /**
     * Returns found document
     * @async
     * @param {String} collection_name
     * @param {Object} query
     * @param {Object} fields_selection
     * @returns {Promise<Object|error>}
     */
    async findDocument(collection_name, query) {
        await this.getConnection();
        const dbo = await this.useDataBase(MongoDbRepository.DATABASE_NAME);
        return dbo.collection(collection_name).findOne(query);
    }


    /**
     * @param {String} collection_name
     * @param {Object} query
     * @param {Object} document
     * @returns {Promise<Object|error>}
     */
    async updateDocument(collection_name, query, document) {
        await this.getConnection();
        const dbo = await this.useDataBase(MongoDbRepository.DATABASE_NAME);
        return dbo.collection(collection_name).updateOne(
            query,
            {
                $set: document,
            }
        );
    }

    /**
     * @param {String} collection_name
     * @param {Object} query
     * @param {Object} document
     * @returns {Promise<Object|error>}
     */
    async replaceDocument(collection_name, query, document) {
        await this.getConnection();
        const dbo = await this.useDataBase(MongoDbRepository.DATABASE_NAME);
        return dbo.collection(collection_name).replaceOne(
            query,
            document
        );
    }


    /**
     * @param {String} collection_name
     * @param {Object} query
     * @returns {Promise<Object|error>}
     */
    async deleteDocument(collection_name, query) {
        await this.getConnection();
        const dbo = await this.useDataBase(MongoDbRepository.DATABASE_NAME);
        return dbo.collection(collection_name)
            .deleteOne(
                query
            );
    }

    /**
     * @param {String} collection_name
     * @param {Object} index_fields
     * @param {Object} index_params
     * @returns {Promise<Object|error>}
     */
    async createIndex(collection_name, index_fields, index_params) {
        await this.getConnection();
        const dbo = await this.useDataBase(MongoDbRepository.DATABASE_NAME);
        return dbo.collection(collection_name)
            .createIndex(
                index_fields,
                index_params
            );
    }


    /**
     * Returns a list of found documents
     * @async
     * @param {String} collection_name
     * @param {Object} query
     * @param {Number} limit
     * @param {Number} offset
     * @param {Object} sort
     * @param {Object} fields_selection
     * @returns {Promise<Object|error>}
     */
    async findDocumentList(collection_name, query, limit, offset, sort, fields_selection) {
        await this.getConnection();
        const dbo = await this.useDataBase(MongoDbRepository.DATABASE_NAME);
        return dbo.collection(collection_name)
            .find(query)
            .sort(sort)
            .project(fields_selection)
            .skip(offset === undefined ? 0 : offset)
            .limit(limit === undefined ? 0 : limit)
            .toArray();
    }

    /**
     * Returns a list of found documents
     * @async
     * @param {String} collection_name
     * @param {Object} query
     * @param {Number} limit
     * @param {Number} offset
     * @param {Object} sort
     * @param {Object} fields_selection
     * @returns {Promise<Object|error>}
     */
    async countDocumentList(collection_name, query, limit, offset) {
        await this.getConnection();
        const dbo = await this.useDataBase(MongoDbRepository.DATABASE_NAME);
        return dbo.collection(collection_name)
            .find(query)
            .skip(offset === undefined ? 0 : offset)
            .limit(limit === undefined ? 0 : limit)
            .count();
    }


    /**
     * Returns a list of found documents
     *
     * @async
     * @param {String} collection_name
     * @param {Object} aggregation
     * @returns {Promise<Object|error>}
     */
    async aggregate(collection_name, aggregation) {
        await this.getConnection();
        const dbo = await this.useDataBase(MongoDbRepository.DATABASE_NAME);
        return dbo.collection(collection_name)
            .aggregate(
                aggregation,
                {
                    allowDiskUse: true,
                }
            )
            .toArray();
    }


    /**
     * Insert single document
     * @async
     * @param {String} collection_name
     * @param {String} document
     * @returns {Promise<Object|error>}
     */
    async dropDatabase() {
        await this.getConnection();
        const dbo = await this.useDataBase(MongoDbRepository.DATABASE_NAME);
        return dbo.dropDatabase();
    }

}

MongoDbRepository.instance = null;

module.exports = {
    MongoDbRepository,
};


