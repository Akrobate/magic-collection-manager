'use strict';

const {
    DataTypes,
    Op,
} = require('sequelize');

const {
    sequelize,
} = require('./connections/Sequelize');

const {
    AbstractSequelizeRepository,
} = require('./AbstractSequelizeRepository');

class CardListRepository extends AbstractSequelizeRepository {

    /**
     * @static
     * @returns {CardListRepository}
     */
    static getInstance() {
        if (CardListRepository.instance === null) {
            CardListRepository.instance = new CardListRepository(
                CardListRepository.getModel()
            );
        }
        return CardListRepository.instance;
    }


    /**
     * @returns {Object}
     */
    static get TYPE_ID() {
        return {
            COLLECTION: 1,
            DECK: 2,
            LIST: 3,
        };
    }


    /**
     * @returns {Sequelize<Object>}
     */
    static getModel() {
        return sequelize.define(
            'CardList',
            {
                id: {
                    allowNull: false,
                    autoIncrement: true,
                    primaryKey: true,
                    type: DataTypes.INTEGER.UNSIGNED,
                },
                user_id: {
                    allowNull: false,
                    type: DataTypes.INTEGER.UNSIGNED,
                    unique: false,
                },
                type_id: {
                    allowNull: false,
                    type: DataTypes.INTEGER.UNSIGNED,
                    unique: false,
                },
                name: {
                    allowNull: true,
                    type: DataTypes.STRING,
                    unique: false,
                },
                created_at: {
                    allowNull: false,
                    type: DataTypes.DATE,
                },
                updated_at: {
                    allowNull: false,
                    type: DataTypes.DATE,
                },
            },
            {
                tableName: 'card_lists',
                underscored: true,
                timestamps: false,
            }
        );
    }

    /**
     * @static
     * @param {Object} criteria
     * @returns {Object}
     */
    static _formatCriteria(criteria) {
        const {
            id,
            id_list,
            user_id,
            user_id_list,
            type_id,
            type_id_list,
        } = criteria;

        const where = {
            [Op.and]: {},
        };

        if (id) {
            where[Op.and].id = {
                [Op.eq]: id,
            };
        }

        if (id_list) {
            where[Op.and].id = {
                [Op.in]: id_list,
            };
        }

        if (user_id) {
            where[Op.and].user_id = {
                [Op.eq]: user_id,
            };
        }

        if (user_id_list) {
            where[Op.and].user_id = {
                [Op.in]: user_id_list,
            };
        }

        if (type_id) {
            where[Op.and].type_id = {
                [Op.eq]: type_id,
            };
        }

        if (type_id_list) {
            where[Op.and].type_id = {
                [Op.in]: type_id_list,
            };
        }

        return {
            where,
        };
    }
}

CardListRepository.instance = null;

module.exports = {
    CardListRepository,
};
