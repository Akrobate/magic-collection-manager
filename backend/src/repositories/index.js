'use strict';

const {
    JsonFileRepository,
} = require('./JsonFileRepository');

const {
    UserRepository,
} = require('./UserRepository');

const {
    MongoDbRepository,
} = require('./MongoDbRepository');

const {
    CardRepository,
} = require('./CardRepository');

const {
    CardListRepository,
} = require('./CardListRepository');

const {
    CardListItemRepository,
} = require('./CardListItemRepository');

module.exports = {
    CardRepository,
    JsonFileRepository,
    MongoDbRepository,
    UserRepository,
    CardListRepository,
    CardListItemRepository,
};
