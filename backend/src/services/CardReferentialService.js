'use strict';

const {
    JsonFileRepository,
} = require('../repositories');

const {
    SOURCE_FILE,
} = require('../constants');

//     CARD_SET_REFERENTAIL
//     CARD_SYMBOL_REFERENTAIL


class CardReferentialService {

    /**
     * Constructor.
     * @param {JsonFileRepository} json_file_repository
     */
    constructor(
        json_file_repository
    ) {
        this.json_file_repository = json_file_repository;

        this.data_source_base_path = `${__dirname}/../../../data/source/`;
        this.static_data_source_base_path = `${__dirname}/../static_data/`;

        this.color_list = null;
        this.set_list = null;

        this.type_list = null;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {CardReferentialService}
     */
    static getInstance() {
        if (CardReferentialService.instance === null) {
            CardReferentialService.instance = new CardReferentialService(
                JsonFileRepository.getInstance()
            );
        }
        return CardReferentialService.instance;
    }


    /**
     * @param {Strring} data_source_base_path
     * @returns {Void}
     */
    setDataSourceBasePath(data_source_base_path) {
        this.data_source_base_path = data_source_base_path;
    }


    /**
     * @returns {Promise<Array>}
     */
    async getCardReferentialColor() {
        if (this.color_list) {
            return this.color_list;
        }

        const simple_color_symbol_list = [
            '{W}',
            '{U}',
            '{B}',
            '{R}',
            '{G}',
            '{C}',
        ];

        const symbol_referential_list = await this.json_file_repository
            .getData(`${this.data_source_base_path}${SOURCE_FILE.CARD_SYMBOL_REFERENTAIL}`);

        this.color_list = symbol_referential_list
            .data
            .filter(
                (symbol_referential) => simple_color_symbol_list.includes(symbol_referential.symbol)
            )
            .map((symbol_referential) => ({
                symbol: symbol_referential.symbol,
                color: symbol_referential.symbol
                    .replace('{', '')
                    .replace('}', ''),
                text: symbol_referential.english,
                svg_uri: symbol_referential.svg_uri,
            }));
        return this.color_list;
    }


    /**
     * @returns {Promise<Array>}
     */
    async getCardReferentialSet() {
        if (this.set_list) {
            return this.set_list;
        }

        const symbol_referential_list = await this.json_file_repository
            .getData(`${this.data_source_base_path}${SOURCE_FILE.CARD_SET_REFERENTAIL}`);

        this.set_list = symbol_referential_list
            .data
            .filter((set) => set.digital === false)
            // .filter((set) => !['p', 't'].includes(set.code[0]))
            .filter((set) => !['p'].includes(set.code[0]))
            .map((set) => {

                const {
                    // object,
                    id,
                    code,
                    name,
                    // uri,
                    // scryfall_uri,
                    // search_uri,
                    released_at,
                    set_type,
                    card_count,
                    parent_set_code,
                    // digital,
                    // nonfoil_only,
                    // foil_only,
                    icon_svg_uri,
                } = set;

                return {
                    id,
                    code,
                    name,
                    released_at,
                    set_type,
                    card_count,
                    parent_set_code: parent_set_code ? parent_set_code : null,
                    icon_svg_uri,
                };
            });
        return this.set_list;
    }


    /**
     * @returns {Promise<Array>}
     */
    async getCardReferentialType() {
        if (this.type_list) {
            return this.type_list;
        }

        this.type_list = await this.json_file_repository
            .getData(`${this.static_data_source_base_path}card-type-referential.json`);

        return this.type_list;
    }


    /**
     * @returns {Promise<Array>}
     */
    async getCardReferentialMetaType() {
        if (this.meta_type_list) {
            return this.meta_type_list;
        }

        this.meta_type_list = await this.json_file_repository
            .getData(`${this.static_data_source_base_path}card-meta-type-referential.json`);

        return this.meta_type_list;
    }

}

CardReferentialService.instance = null;

module.exports = {
    CardReferentialService,
};
