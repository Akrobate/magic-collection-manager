'use strict';

const {
    CustomError,
} = require('../CustomError');
const {
    CardListRepository,
    CardListItemRepository,
} = require('../repositories');

const {
    Acl,
} = require('./commons/Acl');

class CardListService {

    /**
     * Constructor.
     * @param {Acl} acl
     * @param {CardListRepository} card_list_repository
     * @param {CardListItemRepository} card_list_item_repository
     */
    constructor(
        acl,
        card_list_repository,
        card_list_item_repository
    ) {
        this.acl = acl;
        this.card_list_repository = card_list_repository;
        this.card_list_item_repository = card_list_item_repository;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {CardListService}
     */
    static getInstance() {
        if (CardListService.instance === null) {
            CardListService.instance = new CardListService(
                Acl.getInstance(),
                CardListRepository.getInstance(),
                CardListItemRepository.getInstance()
            );
        }
        return CardListService.instance;
    }


    /**
     * @param {Object} user
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async search(user, input) {
        this.acl.checkDataOwner(user.user_id, input.user_id);
        const card_list_list = await this.card_list_repository.search(input);

        const card_list_item_list = await this.card_list_item_repository.search({
            card_list_id_list: card_list_list.map((card_list) => card_list.id),
        });

        return card_list_list.map((card_list) => {

            const filtered_card_list_item_list = card_list_item_list
                .filter((card_list_item) => card_list_item.card_list_id === card_list.id);

            const count = filtered_card_list_item_list.length;
            const count_unique_card_id = [
                ...new Set(
                    filtered_card_list_item_list.map((card_list_item) => card_list_item.card_id)
                ),
            ];
            const count_unique_oracle_id = [
                ...new Set(
                    filtered_card_list_item_list.map((card_list_item) => card_list_item.oracle_id)
                ),
            ];

            return {
                ...card_list,
                count,
                count_unique_card_id,
                count_unique_oracle_id,
            };
        });
    }


    /**
     * @param {Object} user
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    create(user, input) {
        this.acl.checkDataOwner(user.user_id, input.user_id);
        return this.card_list_repository.create(input);
    }


    /**
     * @param {Object} user
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    update(user, input) {
        this.acl.checkDataOwner(user.user_id, input.user_id);
        return this.card_list_repository.update(input);
    }


    /**
     * @param {Object} user
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async delete(user, input) {
        const card_list = await this.card_list_repository.read(input.id);
        this.acl.checkDataOwner(user.user_id, card_list.user_id);

        await this.card_list_item_repository.deleteAll({
            card_list_id: card_list.id,
        });

        return this.card_list_repository.delete(input.id);
    }


    /**
     * @param {Object} user
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async duplicate(user, input) {
        this.acl.checkDataOwner(user.user_id, input.user_id);

        const card_list = await this.card_list_repository.create(input);

        const card_list_item_list = await this.card_list_item_repository.search({
            card_list_id_list: [
                input.card_list_id,
            ],
        });

        for await (const card_list_item of card_list_item_list) {
            await this.card_list_item_repository.create({
                card_list_id: card_list.id,
                oracle_id: card_list_item.oracle_id,
                card_id: card_list_item.card_id,
                state: card_list_item.state,
            });
        }

        return card_list;
    }


    /**
     * @param {Object} user
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async deleteAllItems(user, input) {
        const {
            card_list_id,
        } = input;
        const card_list = await this.card_list_repository.read(card_list_id);
        this.acl.checkDataOwner(user.user_id, card_list.user_id);

        return this.card_list_item_repository.deleteAll({
            card_list_id,
        });
    }


    /**
     * @param {Object} user
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async createItem(user, input) {
        const {
            card_list_id,
            oracle_id,
            card_id,
            state,
            user_id,
        } = input;

        this.acl.checkDataOwner(user.user_id, user_id);
        const card_list = await this.card_list_repository.find({
            id: card_list_id,
            user_id,
        });
        if (!card_list) {
            throw new CustomError(CustomError.ACCESS_FORBIDDEN, 'User not allowed to modify this ressource');
        }
        return this.card_list_item_repository.create({
            card_list_id,
            oracle_id,
            card_id,
            state,
        });
    }


    /**
     * @param {Object} user
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async deleteItem(user, input) {
        const {
            card_list_item_id,
        } = input;

        const collection_user_card = await this.card_list_item_repository
            .read(card_list_item_id);

        const card_list = await this.card_list_repository
            .read(collection_user_card.card_list_id);
        this.acl.checkDataOwner(user.user_id, card_list.user_id);
        return this.card_list_item_repository.delete(input.card_list_item_id);
    }


    /**
     * @param {Object} user
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async searchItem(user, input) {
        this.acl.checkDataOwner(user.user_id, input.user_id);

        const {
            card_list_id,
            card_list_id_list,
            card_list_type_id,
            card_list_type_id_list,
            user_id,
            card_id,
            oracle_id,
            limit,
            offset,
        } = input;

        const card_list_list = await this.card_list_repository.search({
            id: card_list_id,
            id_list: card_list_id_list,
            type_id: card_list_type_id,
            type_id_list: card_list_type_id_list,
            user_id,
        });

        return this.card_list_item_repository.search({
            card_list_id_list: card_list_list.map((card_list) => card_list.id),
            card_id,
            oracle_id,
            limit,
            offset,
        });
    }

}

CardListService.instance = null;

module.exports = {
    CardListService,
};
