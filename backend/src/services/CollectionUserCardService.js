'use strict';

const {
    CardListRepository,
    CardListItemRepository,
} = require('../repositories');

const {
    Acl,
} = require('./commons/Acl');

class CollectionUserCardService {

    /**
     * Constructor.
     * @param {Acl} acl
     * @param {CardListRepository} card_list_repository
     * @param {CardListItemRepository} card_list_item_repository
     */
    constructor(
        acl,
        card_list_repository,
        card_list_item_repository
    ) {
        this.acl = acl;
        this.card_list_repository = card_list_repository;
        this.card_list_item_repository = card_list_item_repository;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {CollectionUserCardService}
     */
    static getInstance() {
        if (CollectionUserCardService.instance === null) {
            CollectionUserCardService.instance = new CollectionUserCardService(
                Acl.getInstance(),
                CardListRepository.getInstance(),
                CardListItemRepository.getInstance()
            );
        }
        return CollectionUserCardService.instance;
    }


    /**
     * @param {Object} user
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async search(user, input) {
        this.acl.checkDataOwner(user.user_id, input.user_id);

        const card_list = await this.card_list_repository.find({
            user_id: input.user_id,
            type_id: CardListRepository.TYPE_ID.COLLECTION,
        });

        return this.card_list_item_repository.search({
            ...input,
            user_id: undefined,
            card_list_id: card_list.id,
        });
    }


    /**
     * @param {Object} user
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async create(user, input) {

        this.acl.checkDataOwner(user.user_id, input.user_id);
        const card_list = await this.card_list_repository.find({
            user_id: input.user_id,
            type_id: CardListRepository.TYPE_ID.COLLECTION,
        });

        return this.card_list_item_repository.create({
            ...input,
            user_id: undefined,
            card_list_id: card_list.id,
        });
    }


    /**
     * @param {Object} user
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async delete(user, input) {
        const collection_user_card = await this.card_list_item_repository.read(input.id);
        const card_list = await this.card_list_repository.read(collection_user_card.card_list_id);
        this.acl.checkDataOwner(user.user_id, card_list.user_id);
        return this.card_list_item_repository.delete(input.id);
    }

}

CollectionUserCardService.instance = null;

module.exports = {
    CollectionUserCardService,
};
