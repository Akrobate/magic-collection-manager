'use strict';

const {
    CardRepository,
} = require('../repositories');


class CardService {

    /**
     * Constructor.
     * @param {CardRepository} card_repository
     */
    constructor(
        card_repository
    ) {
        this.card_repository = card_repository;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {CardService}
     */
    static getInstance() {
        if (CardService.instance === null) {
            CardService.instance = new CardService(
                CardRepository.getInstance()
            );
        }
        return CardService.instance;
    }


    /**
     * @param {Object} input
     * @returns {Promise<Array>}
     */
    async search(input) {
        if (input.limit === undefined) {
            input.limit = 9;
        }
        const card_list = await this.card_repository.search({
            ...input,
        });
        return card_list;
    }


    /**
     * @param {Object} input
     * @returns {Promise<Array>}
     */
    count(input) {
        return this.card_repository.count(input);
    }

}

CardService.instance = null;

module.exports = {
    CardService,
};
