'use strict';

const crypto = require('crypto');
const jwt = require('jsonwebtoken');

const {
    UserRepository,
    CardListRepository,
} = require('../repositories');

const {
    CustomError,
} = require('../CustomError');

const {
    configuration,
} = require('../configuration');


class UserService {

    /**
     * Constructor.
     * @param {UserRepository} user_repository
     * @param {CardListRepository} card_list_repository
     */
    constructor(
        user_repository,
        card_list_repository
    ) {
        this.user_repository = user_repository;
        this.card_list_repository = card_list_repository;

        this.jwt_private_key = configuration.jwt.private_key;
        this.jwt_public_key = configuration.jwt.public_key;
        this.jwt_config = {
            algorithm: configuration.jwt.algorithm,
            expiresIn: configuration.jwt.default_token_duration,
        };
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {UserService}
     */
    static getInstance() {
        if (UserService.instance === null) {
            UserService.instance = new UserService(
                UserRepository.getInstance(),
                CardListRepository.getInstance()
            );
        }
        return UserService.instance;
    }


    /**
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async register(input) {

        const {
            password,
            email,
        } = input;


        const existing_user = await this.user_repository.find({
            email,
        });

        if (existing_user === null) {
            const user = await this.user_repository
                .create(Object.assign({}, input, {
                    password: UserService.hashPassword(password),
                }));
            await this.createCollectionCardList(user.id);
            return user;
        }

        if (existing_user.password !== null) {
            throw new CustomError(CustomError.BAD_PARAMETER, 'Email already exists');
        }

        return this.user_repository.update(
            Object.assign(
                {},
                input,
                {
                    password: UserService.hashPassword(password),
                },
                {
                    id: existing_user.id,
                }
            )
        );
    }

    /**
     * @param {Number} user_id
     * @returns {Promise<Object>}
     */
    async createCollectionCardList(user_id) {
        let collection_card_list = await this.card_list_repository.find({
            type_id: CardListRepository.TYPE_ID.COLLECTION,
            user_id,
        });
        if (collection_card_list === null) {
            collection_card_list = await this.card_list_repository.create({
                type_id: CardListRepository.TYPE_ID.COLLECTION,
                name: null,
                user_id,
            });
        }
        return collection_card_list;
    }

    /**
     * @param {String} password
     * @returns {String}
     */
    static hashPassword(password) {
        return crypto
            .createHash('sha256')
            .update(`${configuration.security.salt}${password}`)
            .digest('base64');
    }


    /**
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async login(input) {
        const {
            email,
            password,
        } = input;

        const user = await this.user_repository.find({
            email,
        });

        if (user === null) {
            throw new CustomError(CustomError.UNAUTHORIZED, 'Bad login or password');
        }

        if (user.password !== UserService.hashPassword(password)) {
            throw new CustomError(CustomError.UNAUTHORIZED, 'Bad login or password');
        }

        const jwt_user_data = {
            user_id: user.id,
            email: user.email,
            access_type: 'full',
        };

        let jwt_token = null;

        try {
            jwt_token = jwt.sign(jwt_user_data, this.jwt_private_key, this.jwt_config);
        } catch (error) {
            throw new CustomError(CustomError.INTERNAL_ERROR, error.message);
        }
        return {
            token: jwt_token,
        };
    }


    /**
     * @param {Object} user
     * @returns {Promise<*|Error>}
     */
    async renewToken(user) {
        const {
            user_id,
            email,
            access_type,
        } = user;

        const user_found = await this.user_repository.find({
            email,
        });

        if (user_found === null) {
            throw new CustomError(CustomError.UNAUTHORIZED, 'Access denied');
        }

        const jwt_user_data = {
            user_id,
            email,
            access_type,
        };

        let jwt_token = null;

        try {
            jwt_token = jwt.sign(jwt_user_data, this.jwt_private_key, this.jwt_config);
        } catch (error) {
            throw new CustomError(CustomError.INTERNAL_ERROR, error.message);
        }
        return {
            token: jwt_token,
        };
    }


    /**
     * @param {Object} user_data
     * @param {Object} input
     * @returns {Promise<*|Error>}
     */
    async read(user_data, input) {
        const {
            id,
        } = input;

        if (user_data.user_id !== input.id) {
            throw new CustomError(CustomError.UNAUTHORIZED, 'User can only read own data');
        }
        const user = await this.user_repository.find({
            id_list: [id],
        });

        if (user === null) {
            throw new CustomError(CustomError.UNAUTHORIZED, 'User data cannot be accessed');
        }

        return user;
    }

}

UserService.instance = null;

module.exports = {
    UserService,
};
