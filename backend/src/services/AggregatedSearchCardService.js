'use strict';

const {
    CustomError,
} = require('../CustomError');
const {
    CardRepository,
    CardListRepository,
    CardListItemRepository,
} = require('../repositories');

const {
    Acl,
} = require('./commons/Acl');

class AggregatedSearchCardService {

    /**
     * Constructor.
     * @param {Acl} acl
     * @param {CardRepository} card_repository
     * @param {CardListRepository} card_list_repository
     * @param {CardListItemRepository} card_list_item_repository
     */
    constructor(
        acl,
        card_repository,
        card_list_repository,
        card_list_item_repository
    ) {
        this.acl = acl;
        this.card_repository = card_repository;
        this.card_list_repository = card_list_repository;
        this.card_list_item_repository = card_list_item_repository;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {AggregatedSearchCardService}
     */
    static getInstance() {
        if (AggregatedSearchCardService.instance === null) {
            AggregatedSearchCardService.instance = new AggregatedSearchCardService(
                Acl.getInstance(),
                CardRepository.getInstance(),
                CardListRepository.getInstance(),
                CardListItemRepository.getInstance()
            );
        }
        return AggregatedSearchCardService.instance;
    }


    /**
     * @todo: First iteration on aggregated search
     * @param {Object} user
     * @param {Object} input
     * @returns {Promise<Array>}
     */
    searchInCollection(user, input) {

        this.acl.checkDataOwner(user.user_id, input.user_id);

        const collection_card_list = this.card_list_repository.find({
            user_id: input.user_id,
            type_id: CardListRepository.TYPE_ID.COLLECTION,
        });

        return this.searchInCardList(user, {
            ...input,
            card_list_id: collection_card_list.id,
        });
    }


    /**
     * @todo: First iteration on aggregated search
     * @param {Object} user
     * @param {Object} input
     * @returns {Promise<Array>}
     */
    async searchInCardList(user, input) {

        this.acl.checkDataOwner(user.user_id, input.user_id);

        if (input.limit === undefined) {
            input.limit = 9;
        }

        const {
            sort_by_count,
            card_repository_sort,
        } = this.formatSortsForRepositories(input);


        // Step search in collection
        const collection_user_card_list = await this
            .card_list_item_repository.search(
                {
                    ...input,
                    // cart_list_id: collection_card_list.id,
                    sort_by_count,
                },
                {
                    limit: undefined,
                    offset: undefined,
                }
            );

        // Step search in card library
        const card_list = await this
            .card_repository.search(
                {
                    ...input,
                    limit: undefined,
                    offset: undefined,
                    sort_list: card_repository_sort,
                    id_list: collection_user_card_list.map((item) => item.card_id),
                }
            );

        let all_matched_card_list = [];
        if (card_repository_sort) {
            all_matched_card_list = this
                .sortResultsOnCardList(card_list, collection_user_card_list);
        } else {
            all_matched_card_list = this
                .sortResultsOnCollectionUserCardList(card_list, collection_user_card_list);
        }

        return all_matched_card_list.slice(input.offset, input.offset + input.limit);
    }


    /**
     * @todo: First iteration on aggregated search
     * @param {Object} user
     * @param {Object} input
     * @returns {Array}
     */
    async countInCardList(user, input) {

        this.acl.checkDataOwner(user.user_id, input.user_id);

        // Step search in collection
        const collection_user_card_list = await this
            .card_list_item_repository.search(
                {
                    ...input,
                },
                {
                    limit: undefined,
                    offset: undefined,
                }
            );

        // Step search in card library
        const card_list = await this
            .card_repository.search(
                {
                    ...input,
                    limit: undefined,
                    offset: undefined,
                    id_list: collection_user_card_list.map((item) => item.card_id),
                }
            );

        return this
            .sortResultsOnCollectionUserCardList(card_list, collection_user_card_list)
            .length;
    }


    /**
     * @param {*} input
     * @return {Object}
     */
    formatSortsForRepositories(input) {
        const {
            sort,
            group_by_oracle_id,
            group_by_card_id,
        } = input;

        let sort_by_count = null;

        if (sort === 'count') {
            sort_by_count = 'asc';
        } else if (sort === '-count') {
            sort_by_count = 'desc';
        }

        if (sort_by_count && !group_by_oracle_id && !group_by_card_id) {
            throw new CustomError(CustomError.BAD_PARAMETER, 'Not able to sort by count withou grouping');
        }

        let card_repository_sort = null;
        if (sort && sort !== 'count' && sort !== '-count') {
            card_repository_sort = [
                sort,
            ];
        }

        return {
            sort_by_count,
            card_repository_sort,
        };
    }


    /**
     * @param {Array} card_list
     * @param {Array} collection_user_card_list
     * @returns {Array}
     */
    sortResultsOnCardList(card_list, collection_user_card_list) {
        const all_matched_card_list = [];
        card_list.forEach((card) => {
            collection_user_card_list
                .filter((collection_user_card) => collection_user_card.card_id === card.id)
                .forEach((collection_user_card) => {
                    all_matched_card_list.push({
                        ...card,
                        count: collection_user_card.count ? collection_user_card.count : 1,
                        card_list_item_id: collection_user_card.id,
                    });
                });
        });
        return all_matched_card_list;
    }


    /**
     * @param {Array} card_list
     * @param {Array} collection_user_card_list
     * @returns {Array}
     */
    sortResultsOnCollectionUserCardList(card_list, collection_user_card_list) {
        const all_matched_card_list = [];
        collection_user_card_list
            .forEach((collection_user_card) => {
                const matched_card = card_list
                    .find((card) => card.id === collection_user_card.card_id);
                if (matched_card) {
                    all_matched_card_list.push({
                        ...matched_card,
                        count: collection_user_card.count ? collection_user_card.count : 1,
                        card_list_item_id: collection_user_card.id,
                    });
                }
            });
        return all_matched_card_list;
    }


    /**
     * @todo: Do or remove it
     * @param {Array<Object>} card_id_object_list
     * @returns {Array<Object>}
     */
    enrichResultWithFormatedData(card_id_object_list) {
        const enriched_list = card_id_object_list;

        // iterate over object lists

        return enriched_list;
    }

}

AggregatedSearchCardService.instance = null;

module.exports = {
    AggregatedSearchCardService,
};
