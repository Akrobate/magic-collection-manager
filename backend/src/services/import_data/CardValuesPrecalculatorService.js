'use strict';

const {
    logger,
} = require('../../logger');

const {
    CardRepository,
} = require('../../repositories');

class CardValuesPrecalculatorService {


    // eslint-disable-next-line require-jsdoc
    constructor(card_repository) {
        this.card_repository = card_repository;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {CardValuesPrecalculatorService}
     */
    static getInstance() {
        if (CardValuesPrecalculatorService.instance === null) {
            CardValuesPrecalculatorService.instance = new CardValuesPrecalculatorService(
                CardRepository.getInstance()
            );
        }
        return CardValuesPrecalculatorService.instance;
    }


    /**
     * @returns {Promise}
     */
    async processCardValuesPrecalculate() {
        const existing_id_list = await this.card_repository.getAllIdList();
        const total = existing_id_list.length;
        let count = 0;
        for (const id of existing_id_list) {

            const [
                card,
            ] = await this.card_repository.search({
                id,
            });

            await this.card_repository.update(card._id, {
                ...card,
                precalculated_value: {
                    ...this.generateTypeObject(card.type_line),
                    color_identity_string: this.generateColorIdentityString(card.color_identity),
                    ptc_score: this.generatePtcScore(card),
                    collector_number: this.generateCollectorNumber(card),
                },
            });

            count++;
            logger.log(`${count} / ${total}`);
        }
    }


    /**
     * @param {String} type_line
     * @returns {Object}
     */
    generateOneType(type_line) {
        const type_line_copy = type_line.trim();
        const [
            type,
            sub_type,
        ] = type_line_copy.split('—');
        return {
            type: type ? type.trim().toLocaleLowerCase() : null,
            sub_type: sub_type ? sub_type.trim().toLocaleLowerCase() : null,
        };
    }


    /**
     * @param {String} type_line
     * @returns {Object}
     */
    generateTypeObject(type_line) {

        const result = {
            type_1: null,
            sub_type_1: null,
            type_2: null,
            sub_type_2: null,
        };

        if (type_line) {
            if (type_line.includes('//')) {
                const type_line_list = type_line.trim().split('//');
                const [
                    type_line_1,
                    type_line_2,
                ] = type_line_list;
                result.type_1 = this.generateOneType(type_line_1).type;
                result.sub_type_1 = this.generateOneType(type_line_1).sub_type;
                result.type_2 = this.generateOneType(type_line_2).type;
                result.sub_type_2 = this.generateOneType(type_line_2).sub_type;
            } else {
                result.type_1 = this.generateOneType(type_line).type;
                result.sub_type_1 = this.generateOneType(type_line).sub_type;
            }
        }
        return result;
    }


    /**
     * @param {Array} color_list
     * @returns {String}
     */
    generateColorIdentityString(color_list) {
        return color_list.join('_');
    }


    /**
     * @param {Array} color_list
     * @returns {String}
     */
    generatePtcScore({
        power,
        toughness,
        cmc,
    }) {
        return (parseInt(power, 10) + parseInt(toughness, 10)) / parseInt(cmc, 10);
    }

    /**
     * @param {Object} card
     * @returns {number}
     */
    generateCollectorNumber({
        collector_number,
    }) {
        if (!collector_number) {
            return null;
        }
        return Number(collector_number);
    }
}

CardValuesPrecalculatorService.instance = null;

module.exports = {
    CardValuesPrecalculatorService,
};
