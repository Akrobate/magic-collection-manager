'use strict';

const fs = require('fs');
const https = require('https');
const {
    logger,
} = require('../../logger');

const {
    ScryfallRepository,
} = require('../../repositories/http');


class CardDataDownloaderService {

    /**
     * Constructor.
     * @param {CardRepository} scryfall_repository
     */
    constructor(
        scryfall_repository
    ) {
        this.scryfall_repository = scryfall_repository;
        this.data_source_base_path = null; // `${__dirname}/../../../data/source/`;
    }

    /**
     * @param {String} data_source_base_path
     * @returns {Void}
     */
    setDataFilePath(data_source_base_path) {
        this.data_source_base_path = data_source_base_path;
    }

    /* istanbul ignore next */
    /**
     * @static
     * @returns {CardDataDownloaderService}
     */
    static getInstance() {
        if (CardDataDownloaderService.instance === null) {
            CardDataDownloaderService.instance = new CardDataDownloaderService(
                ScryfallRepository.getInstance()
            );
        }
        return CardDataDownloaderService.instance;
    }


    /**
     * @returns {Promise}
     */
    async processCardDataDownload() {

        const bulk_data = await this.scryfall_repository.getBulkData();
        const all_cards = bulk_data.data.find((data) => data.type === 'all_cards');

        logger.log(all_cards);

        const url_file = all_cards.download_uri;
        const file_name = url_file.split('/')[url_file.split('/').length - 1];

        return new Promise((resolve, reject) => {
            https.get(url_file, (response) => {
                response.pipe(fs.createWriteStream(`${this.data_source_base_path}${file_name}`));
                let pointer = 0;

                response.on('data', (chunk) => {
                    pointer += (chunk.length / 1048576);
                    logger.log('Downloaded : ', pointer);
                });

                response.on('end', () => {
                    logger.log('ended');
                    return resolve();
                });

                response.on('error', (error) => {
                    logger.log(`error : ${error.message}`);
                    return reject(error);
                });
            });

        });
    }
}

CardDataDownloaderService.instance = null;

module.exports = {
    CardDataDownloaderService,
};
