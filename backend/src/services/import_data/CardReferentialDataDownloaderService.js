'use strict';

// const {
//     logger,
// } = require('../../logger');

const {
    ScryfallRepository,
} = require('../../repositories/http');

const {
    JsonFileRepository,
} = require('../../repositories');

const {
    SOURCE_FILE,
} = require('../../constants');

class CardReferentialDataDownloaderService {

    /**
     * Constructor.
     * @param {ScryfallRepository} scryfall_repository
     * @param {JsonFileRepository} json_file_repository
     */
    constructor(
        scryfall_repository,
        json_file_repository
    ) {
        this.scryfall_repository = scryfall_repository;
        this.json_file_repository = json_file_repository;
        this.data_source_base_path = null;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {CardReferentialDataDownloaderService}
     */
    static getInstance() {
        if (CardReferentialDataDownloaderService.instance === null) {
            CardReferentialDataDownloaderService
                .instance = new CardReferentialDataDownloaderService(
                    ScryfallRepository.getInstance(),
                    JsonFileRepository.getInstance()
                );
        }
        return CardReferentialDataDownloaderService.instance;
    }


    /**
     * @returns {Promise}
     */
    async processDownloadReferentials() {
        const {
            CARD_SET_REFERENTAIL,
            CARD_SYMBOL_REFERENTAIL,
        } = SOURCE_FILE;
        await this.downloadCardSets(`${this.data_source_base_path}${CARD_SET_REFERENTAIL}`);
        await this.downloadCardSymbols(`${this.data_source_base_path}${CARD_SYMBOL_REFERENTAIL}`);
    }


    /**
     * @param {String} data_source_base_path
     * @returns {Void}
     */
    setDataFilePath(data_source_base_path) {
        this.data_source_base_path = data_source_base_path;
    }


    /**
     * @param {String} file
     * @returns {Promise}
     */
    async downloadCardSymbols(file) {
        const data = await this.scryfall_repository.getCardSymbols();
        await this.json_file_repository.saveData(file, data);
    }


    /**
     * @param {String} file
     * @returns {Promise}
     */
    async downloadCardSets(file) {
        const data = await this.scryfall_repository.getCardSets();
        await this.json_file_repository.saveData(file, data);
    }

}

CardReferentialDataDownloaderService.instance = null;

module.exports = {
    CardReferentialDataDownloaderService,
};
