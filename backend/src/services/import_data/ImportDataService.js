'use strict';

const fs = require('fs');
const readline = require('readline');
const {
    CustomError,
} = require('../../CustomError');
const {
    logger,
} = require('../../logger');
const {
    CardRepository,
} = require('../../repositories');


class ImportDataService {

    /**
     * Constructor.
     * @param {CardRepository} card_repository
     */
    constructor(
        card_repository
    ) {
        this.card_repository = card_repository;

        this.data_file_path = '';
        this.data_folder_path = null;
        this.chunk_size = 1000;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {ImportDataService}
     */
    static getInstance() {
        if (ImportDataService.instance === null) {
            ImportDataService.instance = new ImportDataService(
                CardRepository.getInstance()
            );
        }
        return ImportDataService.instance;
    }


    /**
     * @param {String} data_file_path
     * @returns {Void}
     */
    setDataFilePath(data_file_path) {
        this.data_file_path = data_file_path;
    }

    /**
     * @param {String} data_folder_path
     * @returns {Void}
     */
    setDataFolderPath(data_folder_path) {
        this.data_folder_path = data_folder_path;
    }


    /**
     * @returns {Promises}
     */
    async processCreateNewItems() {

        const existing_id_list = await this.card_repository.getAllIdList();
        const existing_id_list_set = new Set(existing_id_list);

        logger.log(existing_id_list.length);
        logger.log(existing_id_list_set.size);

        let chunk = [];
        let inserted_line_counter = 0;

        await this.iterateAndParseLargeJsonFile(async (line_obj, line_counter) => {
            if (!existing_id_list_set.has(line_obj.id)) {
                chunk.push(line_obj);
            }

            if ((line_counter % this.chunk_size) === 0) {
                logger.log('line: ', line_counter, ' - inserted : ', inserted_line_counter);
            }

            if (chunk.length === this.chunk_size) {
                await this.card_repository.createList(chunk);
                inserted_line_counter += chunk.length;
                logger.log('line: ', line_counter, ' - inserted : ', inserted_line_counter);
                chunk = [];
            }
        });

        if (chunk.length > 0) {
            await this.card_repository.createList(chunk);
            inserted_line_counter += chunk.length;
            logger.log('last insert - inserted : ', inserted_line_counter);
            chunk = [];
        }

        logger.log('processCreateNewItems finished');
    }


    /**
     * @returns {Promises}
     */
    async processForceUpdateCreateItems() {

        let inserted_line_counter = 0;
        let updated_line_counter = 0;

        await this.iterateAndParseLargeJsonFile(async (line_object, line_counter) => {
            const [
                card,
            ] = await this.card_repository.search({
                id: line_object.id,
            });
            if (card === undefined) {
                await this.card_repository.create(line_object);
                inserted_line_counter++;
            } else {
                await this.card_repository.replace(card._id, line_object);
                updated_line_counter++;
            }
            logger.log(
                'Inserted : ', inserted_line_counter,
                ' Updated : ', updated_line_counter,
                ' File line : ', line_counter
            );

        });
        logger.log('processForceUpdateCreateItems finished');
    }


    /**
     * @param {String} line
     * @returns {String}
     */
    cleanLine(line) {
        if (line.charAt(line.length - 1) === ',') {
            return line.substring(0, line.length - 1);
        }
        return line;
    }


    /**
     * @param {Function} process_line_function
     * @returns {Promise<String>}
     */
    async iterateAndParseLargeJsonFile(process_line_function) {

        let line_counter = 0;
        const file_stream = fs.createReadStream(this.data_file_path);
        const read_line_interface = readline.createInterface({
            input: file_stream,
            crlfDelay: Infinity,
        });

        for await (const line of read_line_interface) {

            if (line === '[' || line === ']') {
                // eslint-disable-next-line no-continue
                continue;
            }
            line_counter++;
            const line_obj = JSON.parse(this.cleanLine(line));
            await process_line_function(line_obj, line_counter);
        }
    }


    /**
     * @returns {String}
     */
    getLastDownloadedAllCardFileName() {
        if (this.data_folder_path === null) {
            throw new CustomError(CustomError.INTERNAL_ERROR, 'Data folder path must be setted');
        }

        const all_cards_file_list = [];
        fs.readdirSync(this.data_folder_path).forEach((file) => {
            if (file.includes('all-cards')) {
                all_cards_file_list.push(file);
            }
        });

        if (all_cards_file_list.length === 0) {
            throw new CustomError(CustomError.INTERNAL_ERROR, 'Data file not found, you should first download one');
        }

        const all_cards_file_list_sorted = [...all_cards_file_list].sort((a, b) => {
            if (a < b) {
                return 1;
            }
            if (a > b) {
                return -1;
            }
            return 0;
        });

        const [
            last_file_name,
        ] = all_cards_file_list_sorted;

        return last_file_name;
    }

}

ImportDataService.instance = null;

module.exports = {
    ImportDataService,
};
