'use strict';

const {
    CustomError,
} = require('../../CustomError');


class Acl {

    /* istanbul ignore next */
    /**
     * @static
     * @returns {Acl}
     */
    static getInstance() {
        if (Acl.instance === null) {
            Acl.instance = new Acl();
        }
        return Acl.instance;
    }

    /**
     * @param {*} connected_user_id
     * @param {*} target_user_id
     * @returns {Void|Erro}
     */
    checkDataOwner(connected_user_id, target_user_id) {
        if (connected_user_id !== target_user_id) {
            throw new CustomError(CustomError.ACCESS_FORBIDDEN, 'User data access forbiden');
        }
    }
}

Acl.instance = null;

module.exports = {
    Acl,
};
