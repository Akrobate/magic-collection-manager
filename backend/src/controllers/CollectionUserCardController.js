'use strict';

const joi = require('joi');

const HTTP_CODE = require('http-status');

const {
    AbstractController,
} = require('./AbstractController');

const {
    CollectionUserCardService,
} = require('../services');


class CollectionUserCardController extends AbstractController {

    /**
     * @param {CollectionUserCardService} collection_user_card_service
     */
    constructor(
        collection_user_card_service
    ) {
        super();
        this.collection_user_card_service = collection_user_card_service;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {CollectionUserCardController}
     */
    static getInstance() {
        if (CollectionUserCardController.instance === null) {
            CollectionUserCardController.instance = new CollectionUserCardController(
                CollectionUserCardService.getInstance()
            );
        }

        return CollectionUserCardController.instance;
    }


    /**
     * @returns {Object}
     */
    searchCriteriaValidation() {
        return joi
            .object()
            .keys({
                query: joi.object()
                    .keys({
                        limit: joi
                            .number()
                            .optional(),
                        offset: joi
                            .number()
                            .default(0)
                            .optional(),
                    })
                    .required(),
            })
            .unknown(true);
    }

    /**
     * @returns {Object}
     */
    createValidation() {
        return joi
            .object()
            .keys({
                body: joi.object()
                    .keys({
                        oracle_id: joi
                            .string()
                            .required(),
                        card_id: joi
                            .string()
                            .required(),
                        state: joi
                            .number()
                            .optional()
                            .default(3),
                    })
                    .required(),
            })
            .unknown(true);
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async search(request, response) {

        const {
            error,
            value,
        } = this.searchCriteriaValidation()
            .validate(request);

        this.checkValidationError(error);

        const data = await this.collection_user_card_service.search(
            request.jwt_data,
            {
                ...value.query,
                user_id: Number(request.params.user_id),
            }
        );

        return response.status(HTTP_CODE.OK).send({
            collection_user_card_list: data,
        });
    }

    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async delete(request, response) {

        await this.collection_user_card_service.delete(
            request.jwt_data,
            {
                id: Number(request.params.id),
            }
        );

        return response.status(HTTP_CODE.OK).send();
    }

    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async create(request, response) {

        const {
            error,
            value,
        } = this.createValidation()
            .validate(request);

        this.checkValidationError(error);

        const data = await this.collection_user_card_service.create(
            request.jwt_data,
            {
                ...value.body,
                user_id: Number(request.params.user_id),
            }
        );

        return response.status(HTTP_CODE.CREATED).send(data);
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async count(request, response) {

        const {
            error,
            value,
        } = this
            .searchCriteriaValidation()
            .validate(request);

        this.checkValidationError(error);

        const data = await this.collection_user_card_service.count(
            value.query
        );

        return response.status(HTTP_CODE.OK).send({
            card_count: data,
        });
    }

}

CollectionUserCardController.instance = null;

module.exports = {
    CollectionUserCardController,
};
