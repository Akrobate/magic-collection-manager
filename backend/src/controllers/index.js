'use strict';

const {
    UserController,
} = require('./UserController');
const {
    CardController,
} = require('./CardController');
const {
    CardListController,
} = require('./CardListController');
const {
    CollectionUserCardController,
} = require('./CollectionUserCardController');
const {
    AggregatedSearchCardController,
} = require('./AggregatedSearchCardController');
const {
    CardReferentialController,
} = require('./CardReferentialController');

module.exports = {
    CardController,
    UserController,
    CardListController,
    CollectionUserCardController,
    AggregatedSearchCardController,
    CardReferentialController,
};
