'use strict';

const joi = require('joi');

const HTTP_CODE = require('http-status');

const {
    AbstractController,
} = require('./AbstractController');

const {
    CardListService,
} = require('../services');


class CardListController extends AbstractController {

    /**
     * @param {CardListService} card_list_service
     */
    constructor(
        card_list_service
    ) {
        super();
        this.card_list_service = card_list_service;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {CardListController}
     */
    static getInstance() {
        if (CardListController.instance === null) {
            CardListController.instance = new CardListController(
                CardListService.getInstance()
            );
        }

        return CardListController.instance;
    }


    /**
     * @returns {Object}
     */
    searchCriteriaValidation() {
        return joi
            .object()
            .keys({
                query: joi.object()
                    .keys({
                        id_list: joi
                            .array()
                            .items(
                                joi.number()
                            )
                            .optional(),
                        type_id_list: joi
                            .array()
                            .items(
                                joi.number()
                            )
                            .optional(),
                        limit: joi
                            .number()
                            .optional(),
                        offset: joi
                            .number()
                            .default(0)
                            .optional(),
                    })
                    .required(),
            })
            .unknown(true);
    }


    /**
     * @returns {Object}
     */
    createValidation() {
        return joi
            .object()
            .keys({
                body: joi.object()
                    .keys({
                        name: joi
                            .string()
                            .required(),
                        type_id: joi
                            .number()
                            .min(1)
                            .required(),
                    })
                    .required(),
            })
            .unknown(true);
    }


    /**
     * @returns {Object}
     */
    duplicateValidation() {
        return this.createValidation();
    }


    /**
     * @returns {Object}
     */
    updateValidation() {
        return joi
            .object()
            .keys({
                body: joi.object()
                    .keys({
                        name: joi
                            .string()
                            .min(3)
                            .optional(),
                        type_id: joi
                            .number()
                            .min(1)
                            .optional(),
                    })
                    .required(),
            })
            .unknown(true);
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async search(request, response) {

        const {
            error,
            value,
        } = this.searchCriteriaValidation()
            .validate(request);

        this.checkValidationError(error);

        const data = await this.card_list_service.search(
            request.jwt_data,
            {
                ...value.query,
                user_id: Number(request.params.user_id),
            }
        );

        // Only test porpose (test front loading management)
        // await new Promise((resolve) => setTimeout(() => resolve(), 3000));

        return response.status(HTTP_CODE.OK).send({
            card_list_list: data,
        });
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async create(request, response) {

        const {
            error,
            value,
        } = this.createValidation()
            .validate(request);

        this.checkValidationError(error);

        const data = await this.card_list_service.create(
            request.jwt_data,
            {
                ...value.body,
                user_id: Number(request.params.user_id),
            }
        );

        return response.status(HTTP_CODE.CREATED).send(data);
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async duplicate(request, response) {

        const {
            error,
            value,
        } = this.duplicateValidation()
            .validate(request);

        this.checkValidationError(error);

        const data = await this.card_list_service.duplicate(
            request.jwt_data,
            {
                ...value.body,
                user_id: Number(request.params.user_id),
                card_list_id: Number(request.params.id),
            }
        );

        return response.status(HTTP_CODE.CREATED).send(data);
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async update(request, response) {

        const {
            error,
            value,
        } = this.updateValidation()
            .validate(request);

        this.checkValidationError(error);

        const {
            id,
        } = request.params;

        const data = await this.card_list_service.update(
            request.jwt_data,
            {
                ...value.body,
                id,
                user_id: Number(request.params.user_id),
            }
        );

        return response.status(HTTP_CODE.OK).send(data);
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async delete(request, response) {

        await this.card_list_service.delete(
            request.jwt_data,
            {
                id: Number(request.params.id),
            }
        );

        return response.status(HTTP_CODE.OK).send();
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async deleteItem(request, response) {

        const {
            card_list_id,
            card_list_item_id,
        } = request.params;

        await this.card_list_service.deleteItem(
            request.jwt_data,
            {
                card_list_id: Number(card_list_id),
                card_list_item_id: Number(card_list_item_id),
            }
        );

        return response.status(HTTP_CODE.OK).send();
    }


    /**
     * @returns {Object}
     */
    createItemValidation() {
        return joi
            .object()
            .keys({
                body: joi.object()
                    .keys({
                        oracle_id: joi
                            .string()
                            .required(),
                        card_id: joi
                            .string()
                            .required(),
                        state: joi
                            .number()
                            .optional()
                            .default(3),
                    })
                    .required(),
            })
            .unknown(true);
    }


    /**
     * @returns {Object}
     */
    searchItemCriteriaValidation() {
        return joi
            .object()
            .keys({
                query: joi.object()
                    .keys({
                        limit: joi
                            .number()
                            .optional(),
                        offset: joi
                            .number()
                            .default(0)
                            .optional(),
                        card_id: joi
                            .string()
                            .optional(),
                        card_list_id: joi
                            .number()
                            .optional(),
                        card_list_id_list: joi
                            .array()
                            .items(
                                joi.number()
                            )
                            .optional(),
                        card_list_type_id: joi
                            .number()
                            .optional(),
                        card_list_type_id_list: joi
                            .array()
                            .items(
                                joi.number()
                            )
                            .optional(),
                    })
                    .required(),
            })
            .unknown(true);
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async createItem(request, response) {
        const {
            error,
            value,
        } = this.createItemValidation().validate(request);

        this.checkValidationError(error);

        const {
            card_list_id,
            card_list_item_id,
            user_id,
        } = request.params;

        const data = await this.card_list_service.createItem(
            request.jwt_data,
            {
                ...value.body,
                card_list_id: Number(card_list_id),
                card_list_item_id: Number(card_list_item_id),
                user_id: Number(user_id),
            }
        );

        return response.status(HTTP_CODE.CREATED).send(data);
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async count(request, response) {

        const {
            error,
            value,
        } = this
            .searchCriteriaValidation()
            .validate(request);

        this.checkValidationError(error);

        const data = await this.card_list_service.count(
            value.query
        );

        return response.status(HTTP_CODE.OK).send({
            card_count: data,
        });
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async searchItem(request, response) {

        const {
            error,
            value,
        } = this.searchItemCriteriaValidation()
            .validate(request);

        this.checkValidationError(error);

        const data = await this.card_list_service.searchItem(
            request.jwt_data,
            {
                ...value.query,
                user_id: Number(request.params.user_id),
                card_list_id: Number(request.params.card_list_id),
            }
        );

        return response.status(HTTP_CODE.OK).send({
            card_list_item_list: data,
        });
    }

    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async searchCardListItem(request, response) {

        const {
            error,
            value,
        } = this.searchItemCriteriaValidation()
            .validate(request);

        this.checkValidationError(error);

        const data = await this.card_list_service.searchItem(
            request.jwt_data,
            {
                ...value.query,
                user_id: Number(request.params.user_id),
            }
        );

        return response.status(HTTP_CODE.OK).send({
            card_list_item_list: data,
        });
    }

}

CardListController.instance = null;

module.exports = {
    CardListController,
};
