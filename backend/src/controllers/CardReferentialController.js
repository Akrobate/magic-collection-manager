'use strict';

const HTTP_CODE = require('http-status');

const {
    AbstractController,
} = require('./AbstractController');

const {
    CardReferentialService,
} = require('../services');


class CardReferentialController extends AbstractController {

    /**
     * @param {CardReferentialService} card_referential_service
     */
    constructor(
        card_referential_service
    ) {
        super();
        this.card_referential_service = card_referential_service;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {CardReferentialController}
     */
    static getInstance() {
        if (CardReferentialController.instance === null) {
            CardReferentialController.instance = new CardReferentialController(
                CardReferentialService.getInstance()
            );
        }

        return CardReferentialController.instance;
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async getCardReferentialColor(request, response) {

        const data = await this.card_referential_service.getCardReferentialColor();

        return response.status(HTTP_CODE.OK).send({
            color_list: data,
        });
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async getCardReferentialSet(request, response) {

        const data = await this.card_referential_service.getCardReferentialSet();

        return response.status(HTTP_CODE.OK).send({
            set_list: data,
        });
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async getCardReferentialType(request, response) {

        const data = await this.card_referential_service.getCardReferentialType();

        return response.status(HTTP_CODE.OK).send({
            type_list: data,
        });
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async getCardReferentialMetaType(request, response) {

        const data = await this.card_referential_service.getCardReferentialMetaType();

        return response.status(HTTP_CODE.OK).send({
            meta_type_list: data,
        });
    }

}

CardReferentialController.instance = null;

module.exports = {
    CardReferentialController,
};
