'use strict';

const joi = require('joi');

const HTTP_CODE = require('http-status');

const {
    AbstractCardController,
} = require('./AbstractCardController');

const {
    CardService,
} = require('../services');


class CardController extends AbstractCardController {

    /**
     * @param {CardService} card_service
     */
    constructor(
        card_service
    ) {
        super();
        this.card_service = card_service;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {CardController}
     */
    static getInstance() {
        if (CardController.instance === null) {
            CardController.instance = new CardController(
                CardService.getInstance()
            );
        }

        return CardController.instance;
    }


    /**
     * @returns {Object}
     */
    searchCriteriaValidation() {
        return joi
            .object()
            .keys({
                query: joi.object()
                    .keys({
                        ...this.searchCardGenericPropertiesValidation(),
                        sort: joi
                            .string()
                            .optional(),
                        limit: joi
                            .number()
                            .optional(),
                        offset: joi
                            .number()
                            .default(0)
                            .optional(),
                    })
                    .required(),
            })
            .unknown(true);
    }

    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async search(request, response) {

        const {
            error,
            value,
        } = this
            .searchCriteriaValidation()
            .validate(request);

        this.checkValidationError(error);

        const data = await this.card_service.search(
            value.query
        );

        return response.status(HTTP_CODE.OK).send({
            card_list: data,
        });
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async count(request, response) {

        const {
            error,
            value,
        } = this
            .searchCriteriaValidation()
            .validate(request);

        this.checkValidationError(error);

        const data = await this.card_service.count(
            value.query
        );

        return response.status(HTTP_CODE.OK).send({
            card_count: data,
        });
    }

}

CardController.instance = null;

module.exports = {
    CardController,
};
