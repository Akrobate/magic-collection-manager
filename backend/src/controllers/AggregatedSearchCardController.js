'use strict';

const joi = require('joi');

const HTTP_CODE = require('http-status');

const {
    AbstractCardController,
} = require('./AbstractCardController');

const {
    AggregatedSearchCardService,
} = require('../services');

class AggregatedSearchCardController extends AbstractCardController {

    /**
     * @param {CollectionUserCardService} aggregated_search_card_service
     */
    constructor(
        aggregated_search_card_service
    ) {
        super();
        this.aggregated_search_card_service = aggregated_search_card_service;
    }


    /* istanbul ignore next */
    /**
     * @static
     * @returns {AggregatedSearchCardController}
     */
    static getInstance() {
        if (AggregatedSearchCardController.instance === null) {
            AggregatedSearchCardController.instance = new AggregatedSearchCardController(
                AggregatedSearchCardService.getInstance()
            );
        }

        return AggregatedSearchCardController.instance;
    }


    /**
     * @returns {Object}
     */
    searchCriteriaValidation() {
        return joi
            .object()
            .keys({
                query: joi.object()
                    .keys({
                        ...this.searchCardGenericPropertiesValidation(),
                        group_by_card_id: joi
                            .boolean()
                            .optional(),
                        group_by_oracle_id: joi
                            .boolean()
                            .optional(),
                        sort: joi
                            .string()
                            .optional(),
                        limit: joi
                            .number()
                            .optional(),
                        offset: joi
                            .number()
                            .default(0)
                            .optional(),
                    })
                    .required(),
            })
            .unknown(true);
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async searchInCollection(request, response) {
        const {
            error,
            value,
        } = this.searchCriteriaValidation()
            .validate(request);

        this.checkValidationError(error);

        const data = await this.aggregated_search_card_service.searchInCollection(
            request.jwt_data,
            {
                ...value.query,
                user_id: Number(request.params.user_id),
            }
        );

        return response.status(HTTP_CODE.OK).send({
            card_list: data,
        });
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async searchInCardList(request, response) {
        const {
            error,
            value,
        } = this.searchCriteriaValidation()
            .validate(request);

        this.checkValidationError(error);

        const data = await this.aggregated_search_card_service.searchInCardList(
            request.jwt_data,
            {
                ...value.query,
                user_id: Number(request.params.user_id),
            }
        );

        return response.status(HTTP_CODE.OK).send({
            card_list: data,
        });
    }


    /**
     * @param {express.Request} request
     * @param {express.Response} response
     * @returns {Promise<*|Error>}
     */
    async countInCardList(request, response) {
        const {
            error,
            value,
        } = this.searchCriteriaValidation()
            .validate(request);

        this.checkValidationError(error);

        const data = await this.aggregated_search_card_service.countInCardList(
            request.jwt_data,
            {
                ...value.query,
                user_id: Number(request.params.user_id),
            }
        );

        return response.status(HTTP_CODE.OK).send({
            card_count: data,
        });
    }

}

AggregatedSearchCardController.instance = null;

module.exports = {
    AggregatedSearchCardController,
};
