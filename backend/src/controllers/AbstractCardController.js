'use strict';

const joi = require('joi');

const {
    AbstractController,
} = require('./AbstractController');

class AbstractCardController extends AbstractController {


    /**
     * @returns {Object}
     */
    searchCardGenericPropertiesValidation() {
        return {
            id: joi
                .string()
                .optional(),
            id_list: joi
                .array()
                .items(
                    joi.string()
                )
                .optional(),
            card_list_id: joi
                .number()
                .optional(),
            collector_number: joi
                .string()
                .optional(),
            collector_number_list: joi
                .array()
                .items(
                    joi.string()
                )
                .optional(),
            color_identity_list: joi
                .array()
                .items(
                    joi.string()
                )
                .optional(),
            keyword_list: joi
                .array()
                .items(
                    joi.string()
                )
                .optional(),
            lang: joi
                .string()
                .optional(),
            lang_list: joi
                .array()
                .items(
                    joi.string()
                )
                .optional(),
            quick_search: joi
                .string()
                .optional(),
            set: joi
                .string()
                .optional(),
            set_list: joi
                .array()
                .items(
                    joi.string()
                )
                .optional(),
            type_list: joi
                .array()
                .items(
                    joi.string()
                )
                .optional(),
            sub_type_list: joi
                .array()
                .items(
                    joi.string()
                )
                .optional(),
            oracle_id: joi
                .string()
                .optional(),
            oracle_id_list: joi
                .array()
                .items(
                    joi.string()
                )
                .optional(),
        };
    }
}

module.exports = {
    AbstractCardController,
};
