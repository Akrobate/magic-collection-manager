'use strict';

const constants = {
    SOURCE_FILE: {
        CARD_SET_REFERENTAIL: 'card-set-referential.json',
        CARD_SYMBOL_REFERENTAIL: 'card-symbol-referential.json',
    },
};

module.exports = constants;
