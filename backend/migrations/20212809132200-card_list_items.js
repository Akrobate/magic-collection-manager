'use strict';

module.exports = {
    down: (query_interface) => query_interface
        .dropTable('card_list_items'),
    up: (query_interface, sequelize) => query_interface
        .createTable(
            'card_list_items',
            {
                id: {
                    allowNull: false,
                    autoIncrement: true,
                    primaryKey: true,
                    type: sequelize.INTEGER.UNSIGNED,
                },
                card_list_id: {
                    allowNull: false,
                    type: sequelize.INTEGER.UNSIGNED,
                    unique: false,
                },
                card_id: {
                    allowNull: false,
                    type: sequelize.STRING,
                    unique: false,
                },
                oracle_id: {
                    allowNull: false,
                    type: sequelize.STRING,
                    unique: false,
                },
                state: {
                    allowNull: false,
                    type: sequelize.INTEGER.UNSIGNED,
                    unique: false,
                },
                created_at: {
                    allowNull: false,
                    type: sequelize.DATE,
                },
                updated_at: {
                    allowNull: false,
                    type: sequelize.DATE,
                },
            },
            {
                charset: 'utf8',
                collate: 'utf8_general_ci',
            }
        ),
};
