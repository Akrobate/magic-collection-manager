'use strict';

const superTest = require('supertest');
const qs = require('qs');
const HTTP_CODE = require('http-status');

const {
    expect,
} = require('chai');

const {
    app,
} = require('../../src/app');


const superApp = superTest(app);

describe('Cards Search (Mongodb)', () => {

    it('Should be able to search a card (limit 1)', async () => {
        await superApp
            .get('/api/v1/cards')
            .query(qs.stringify({
                limit: 1,
            }))
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('card_list');
                expect(response.body.card_list).to.have.lengthOf(1);
            });
    });

    it('Should be able to search a card default limit 9', async () => {
        await superApp
            .get('/api/v1/cards')
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('card_list');
                expect(response.body.card_list).to.have.lengthOf(9);
            });
    });


    it('Should be able to quick search cards', async () => {
        await superApp
            .get('/api/v1/cards')
            .query(qs.stringify({
                // quick_search: 'spider',
                quick_search: 'araignée',
                // limit: 10,
            }))
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('card_list');
                // console.log(response.body);
                // expect(response.body.card_list).to.have.lengthOf(10);
                // const name_list = response.body.card_list.map((card) => card.name);
                // const printed_name_list = response.body.card_list.map((card) => card.printed_name);
                // console.log(name_list);
                // console.log(printed_name_list);
                // console.log(printed_name_list.length);
            });
    });


    it('Should be able to count cards', async () => {
        await superApp
            .get('/api/v1/cards/count')
            .query(qs.stringify({
                quick_search: 'araignée',
            }))
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body.card_count).to.equal(3);
            });
    });


    it('Should be able to quick search cards with lang criteria', async () => {
        await superApp
            .get('/api/v1/cards')
            .query(qs.stringify({
                quick_search: 'spider',
                lang: 'fr',
            }))
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('card_list');

                const {
                    card_list,
                } = response.body;

                card_list.forEach((card) => {
                    expect(card).to.have.property('lang', 'fr');
                });
            });
    });

    it('Should be able to search cards with color_identity_list criteria', async () => {
        await superApp
            .get('/api/v1/cards')
            .query(qs.stringify({
                color_identity_list: ['G', 'B'],
            }))
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('card_list');

                const {
                    card_list,
                } = response.body;

                card_list.forEach((card) => {
                    expect(card).to.have.property('color_identity');
                    expect(card.color_identity).to.include('G');
                });
            });
    });

    it('Should be able to search cards in card_faces with quick_search criteria', async () => {
        await superApp
            .get('/api/v1/cards')
            .query(qs.stringify({
                quick_search: 'Emprunteur intrépide',
            }))
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('card_list');
                expect(response.body.card_list).to.be.an('Array')
                    .and.to.have.lengthOf(1);

                const [
                    card,
                ] = response.body.card_list;

                expect(card).to.have.property('id', '003ceff6-d8a0-4ac6-a1f2-64553b230f06');
            });
    });

});
