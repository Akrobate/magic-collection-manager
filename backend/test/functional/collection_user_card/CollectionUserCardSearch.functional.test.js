'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');
const {
    expect,
} = require('chai');
const {
    app,
} = require('../../../src/app');

const {
    user_seed_1,
    user_seed_2,
    collection_user_card_list_seed_1,
    collection_user_card_list_seed_2,
    collection_user_card_seed_1,
    collection_user_card_seed_2,
    collection_user_card_seed_3,
    collection_user_card_seed_4,
    collection_user_card_seed_5,
} = require('../../test_seeds/collection_user_card_seeds');

const {
    DataSeeder,
} = require('../../test_helpers/DataSeeder');

const superApp = superTest(app);

describe('CollectionUserCardSearch', () => {

    before(async () => {
        await DataSeeder.truncate('UserRepository');
        await DataSeeder.truncate('CardListRepository');
        await DataSeeder.truncate('CardListItemRepository');

        await DataSeeder.createUserHashPassword(user_seed_1);
        await DataSeeder.createUserHashPassword(user_seed_2);

        await DataSeeder.create('CardListRepository', collection_user_card_list_seed_1);
        await DataSeeder.create('CardListRepository', collection_user_card_list_seed_2);

        await DataSeeder.create('CardListItemRepository', collection_user_card_seed_1);
        await DataSeeder.create('CardListItemRepository', collection_user_card_seed_2);
        await DataSeeder.create('CardListItemRepository', collection_user_card_seed_3);
        await DataSeeder.create('CardListItemRepository', collection_user_card_seed_4);
        await DataSeeder.create('CardListItemRepository', collection_user_card_seed_5);

    });


    it('Should be able to search collection user card', async () => {
        await superApp
            .get(`/api/v1/users/${user_seed_1.id}/collection-user-cards`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_1)}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('collection_user_card_list');
                const {
                    collection_user_card_list,
                } = response.body;
                expect(collection_user_card_list).to.be.an('Array').and.to.have.lengthOf(3);
            });
    });


    it('Should not be able to search collection of other user', async () => {
        await superApp
            .get(`/api/v1/users/${user_seed_2.id}/collection-user-cards`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_1)}`)
            .expect(HTTP_CODE.FORBIDDEN);
    });


    it('Should be able to add collection user card', async () => {
        await superApp
            .post(`/api/v1/users/${user_seed_1.id}/collection-user-cards`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_1)}`)
            .send({
                oracle_id: 'ORACLE_ID_ADDED_BY_TEST',
                card_id: 'CARD_ID_ADDED_BY_TEST',
                state: 3,
            })
            .expect(HTTP_CODE.CREATED)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('card_list_id', collection_user_card_list_seed_1.id);
                expect(response.body).to.have.property('oracle_id', 'ORACLE_ID_ADDED_BY_TEST');
                expect(response.body).to.have.property('card_id', 'CARD_ID_ADDED_BY_TEST');
                expect(response.body).to.have.property('state', 3);
            });
    });


    it('Should be able to delete collection user card', async () => {
        await superApp
            .get(`/api/v1/users/${user_seed_2.id}/collection-user-cards`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_2)}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body.collection_user_card_list).to.be.an('Array').and.to.have.lengthOf(2);
            });

        await superApp
            .delete(`/api/v1/users/${user_seed_2.id}/collection-user-cards/${collection_user_card_seed_5.id}`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_2)}`)
            .expect(HTTP_CODE.OK);

        await superApp
            .get(`/api/v1/users/${user_seed_2.id}/collection-user-cards`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_2)}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body.collection_user_card_list).to.be.an('Array').and.to.have.lengthOf(1);
            });
    });

    it('Should not be able to delete collection user card of other user', async () => {
        await superApp
            .delete(`/api/v1/users/${user_seed_1.id}/collection-user-cards/${collection_user_card_seed_4.id}`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_1)}`)
            .expect(HTTP_CODE.FORBIDDEN);

    });

});
