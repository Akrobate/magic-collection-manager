'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');
const {
    expect,
} = require('chai');
const {
    app,
} = require('../../../src/app');

const {
    user_seed_1,
} = require('../../test_seeds/collection_user_card_seeds');

const {
    deck_green,
} = require('../../test_seeds/decks');

const {
    DataSeeder,
} = require('../../test_helpers/DataSeeder');

const superApp = superTest(app);

const card_list_seed_1 = {
    id: 234,
    name: 'Deck to duplicate',
    type_id: 2,
    user_id: user_seed_1.id,
};

describe('CardListDuplication', () => {

    before(async () => {
        await DataSeeder.truncate('UserRepository');
        await DataSeeder.truncate('CardListRepository');
        await DataSeeder.truncate('CardListItemRepository');
        await DataSeeder.createUserHashPassword(user_seed_1);
        await DataSeeder.create('CardListRepository', card_list_seed_1);
        await DataSeeder.loadDecksSeedInCardListItemRepository(deck_green, card_list_seed_1.id);
    });


    it('Should be able to search card lists', async () => {
        let duplicated_card_list_id = null;
        await superApp
            .post(`/api/v1/users/${user_seed_1.id}/card-lists/${card_list_seed_1.id}/duplicate`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_1)}`)
            .send({
                name: 'Duplicated list',
                type_id: 2,
            })
            .expect(HTTP_CODE.CREATED)
            .expect((response) => {
                expect(response).to.have.property('body');
                const {
                    body,
                } = response;
                duplicated_card_list_id = body.id;
            });

        await superApp
            .get(`/api/v1/users/${user_seed_1.id}/card-lists/${duplicated_card_list_id}/items`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_1)}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                const {
                    card_list_item_list,
                } = response.body;

                deck_green.deck.forEach((card_item) => {
                    const found = card_list_item_list.find(
                        (duplicated_card_item) => card_item.card_id === duplicated_card_item.card_id
                    );
                    expect(found).not.to.equal(undefined);
                });

                let total_deck = 0;
                deck_green.deck.forEach((item) => {
                    total_deck += item.count;
                });
                expect(total_deck).to.equal(card_list_item_list.length);
            });
    });


});
