'use strict';

const qs = require('qs');
const superTest = require('supertest');
const HTTP_CODE = require('http-status');
const {
    expect,
} = require('chai');
const {
    app,
} = require('../../../src/app');

const {
    user_seed_1,
    user_seed_2,
} = require('../../test_seeds/collection_user_card_seeds');

const {
    DataSeeder,
} = require('../../test_helpers/DataSeeder');

const superApp = superTest(app);

const card_list_seed_1 = {
    id: 50,
    name: 'First test type 2',
    type_id: 2,
    user_id: user_seed_1.id,
};

const card_list_seed_4 = {
    id: 100,
    name: 'Belonds to user 2',
    type_id: 2,
    user_id: user_seed_2.id,
};

const card_list_item_seed_1 = {
    id: 900,
    card_list_id: card_list_seed_1.id,
    oracle_id: 'ORACLE_ID_SEED_4_SHOULD_BE_REMOVED',
    card_id: 'CARD_ID_SEED_4_SHOULD_BE_REMOVED',
    state: 3,
};

// Araignée sentinelle - fr - set cmr
const card_list_item_seed_2 = {
    card_list_id: card_list_seed_4.id,
    oracle_id: 'a37c9b13-dc45-4b55-a63d-5314abde7aab',
    card_id: '41e0f8ba-75ab-48ef-8680-7977563b8205',
    state: 1,
};

// Araignée sentinelle - fr - set m13
const card_list_item_seed_3 = {
    card_list_id: card_list_seed_4.id,
    oracle_id: 'a37c9b13-dc45-4b55-a63d-5314abde7aab',
    card_id: '6c0c074d-2e07-4bc9-97c6-250f972c44c0',
    state: 1,
};

// Araignée sentinelle - fr - set m13 Double
const card_list_item_seed_4 = {
    card_list_id: card_list_seed_4.id,
    oracle_id: 'a37c9b13-dc45-4b55-a63d-5314abde7aab',
    card_id: '6c0c074d-2e07-4bc9-97c6-250f972c44c0',
    state: 2,
};

describe('CollectionUserCardSearch', () => {

    before(async () => {
        await DataSeeder.truncate('UserRepository');
        await DataSeeder.truncate('CardListRepository');
        await DataSeeder.truncate('CardListItemRepository');

        await DataSeeder.createUserHashPassword(user_seed_1);
        await DataSeeder.createUserHashPassword(user_seed_2);

        await DataSeeder.create('CardListRepository', card_list_seed_1);
        await DataSeeder.create('CardListRepository', card_list_seed_4);
        await DataSeeder.create('CardListItemRepository', card_list_item_seed_1);
        await DataSeeder.create('CardListItemRepository', card_list_item_seed_2);
        await DataSeeder.create('CardListItemRepository', card_list_item_seed_3);
        await DataSeeder.create('CardListItemRepository', card_list_item_seed_4);

    });

    it('Should be able to add a card list item', async () => {
        await superApp
            .post(`/api/v1/users/${user_seed_1.id}/card-lists/${card_list_seed_1.id}/items`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_1)}`)
            .send({
                oracle_id: 'AN ORACLE ID',
                card_id: 'A CARD ID',
                state: 1,
            })
            .expect(HTTP_CODE.CREATED)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('oracle_id', 'AN ORACLE ID');
                expect(response.body).to.have.property('card_id', 'A CARD ID');
                expect(response.body).to.have.property('state', 1);
            });
    });

    it('Should be able to delete a card list item', async () => {
        await superApp
            .delete(`/api/v1/users/${user_seed_1.id}/card-lists/${card_list_seed_1.id}/items/${card_list_item_seed_1.id}`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_1)}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');

            });
    });

    it('Should be able to search item in card list item', async () => {
        await superApp
            .get(`/api/v1/users/${user_seed_2.id}/card-lists/${card_list_seed_4.id}/items`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_2)}`)
            .query(qs.stringify({
                card_id: card_list_item_seed_4.card_id,
            }))
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('card_list_item_list');
                const {
                    card_list_item_list,
                } = response.body;
                expect(card_list_item_list).to.be.an('Array').and.to.have.lengthOf(2);
                expect(card_list_item_list[0].card_id).to.equal(card_list_item_seed_4.card_id);
                expect(card_list_item_list[1].card_id).to.equal(card_list_item_seed_4.card_id);
            });
    });


    it('Should be able to search item in card list item', async () => {
        await superApp
            .get(`/api/v1/users/${user_seed_2.id}/card-lists/${card_list_seed_4.id}/items`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_2)}`)
            .query(qs.stringify({}))
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('card_list_item_list');
                const {
                    card_list_item_list,
                } = response.body;

                expect(card_list_item_list).to.be.an('Array').and.to.have.lengthOf(3);
            });
    });


});
