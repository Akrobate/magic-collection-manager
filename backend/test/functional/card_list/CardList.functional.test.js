'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');
const {
    expect,
} = require('chai');
const {
    app,
} = require('../../../src/app');

const {
    user_seed_1,
    user_seed_2,
} = require('../../test_seeds/collection_user_card_seeds');

const {
    DataSeeder,
} = require('../../test_helpers/DataSeeder');

const superApp = superTest(app);

const card_list_seed_1 = {
    name: 'First test type 2',
    type_id: 2,
    user_id: user_seed_1.id,
};
const card_list_seed_2 = {
    name: 'Second test type 2',
    type_id: 2,
    user_id: user_seed_1.id,
};
const card_list_seed_3 = {
    name: 'Type 3',
    type_id: 3,
    user_id: user_seed_1.id,
};
const card_list_seed_4 = {
    id: 100,
    name: 'Belonds to user 2',
    type_id: 2,
    user_id: user_seed_2.id,
};
const card_list_seed_5 = {
    id: 101,
    name: 'Belonds to user 2 and will be deleted',
    type_id: 2,
    user_id: user_seed_2.id,
};
const card_list_seed_6 = {
    id: 102,
    name: 'Belonds to user 2 and will be updated',
    type_id: 2,
    user_id: user_seed_2.id,
};

describe('CollectionUserCardSearch', () => {

    before(async () => {
        await DataSeeder.truncate('UserRepository');
        await DataSeeder.truncate('CardListRepository');
        await DataSeeder.truncate('CardListItemRepository');

        await DataSeeder.createUserHashPassword(user_seed_1);
        await DataSeeder.createUserHashPassword(user_seed_2);

        await DataSeeder.create('CardListRepository', card_list_seed_1);
        await DataSeeder.create('CardListRepository', card_list_seed_2);
        await DataSeeder.create('CardListRepository', card_list_seed_3);
        await DataSeeder.create('CardListRepository', card_list_seed_4);
        await DataSeeder.create('CardListRepository', card_list_seed_5);
        await DataSeeder.create('CardListRepository', card_list_seed_6);

    });


    it('Should be able to search card lists', async () => {
        await superApp
            .get(`/api/v1/users/${user_seed_1.id}/card-lists`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_1)}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('card_list_list');
                const {
                    card_list_list,
                } = response.body;
                expect(card_list_list).to.be.an('Array').and.to.have.lengthOf(3);
                card_list_list.forEach((card_list) => {
                    expect(card_list).to.have.property('count');
                });
            });
    });


    it('Should not be able to search card list of other user', async () => {
        await superApp
            .get(`/api/v1/users/${user_seed_2.id}/card-lists`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_1)}`)
            .expect(HTTP_CODE.FORBIDDEN);
    });


    it('Should be able to add a card list', async () => {
        await superApp
            .post(`/api/v1/users/${user_seed_1.id}/card-lists`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_1)}`)
            .send({
                name: 'SOME NAME',
                type_id: 2,
            })
            .expect(HTTP_CODE.CREATED)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('name', 'SOME NAME');
                expect(response.body).to.have.property('type_id', 2);
            });
    });


    it('Should be able to update a card list', async () => {
        await superApp
            .patch(`/api/v1/users/${user_seed_2.id}/card-lists/${card_list_seed_6.id}`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_2)}`)
            .send({
                name: 'Was updated',
            })
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('name', 'Was updated');
                expect(response.body).to.have.property('type_id', card_list_seed_6.type_id);
            });
        await superApp
            .patch(`/api/v1/users/${user_seed_2.id}/card-lists/${card_list_seed_6.id}`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_2)}`)
            .send({
                type_id: 3,
            })
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('name', 'Was updated');
                expect(response.body).to.have.property('type_id', 3);
            });
    });


    it('Should be able to delete a user card list', async () => {
        await superApp
            .get(`/api/v1/users/${user_seed_2.id}/card-lists`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_2)}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body.card_list_list).to.be.an('Array').and.to.have.lengthOf(3);
            });

        await superApp
            .delete(`/api/v1/users/${user_seed_2.id}/card-lists/${card_list_seed_5.id}`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_2)}`)
            .expect(HTTP_CODE.OK);

        await superApp
            .get(`/api/v1/users/${user_seed_2.id}/card-lists`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_2)}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body.card_list_list).to.be.an('Array').and.to.have.lengthOf(2);
            });
    });

    it('Should not be able to delete a user card list of other user', async () => {
        await superApp
            .delete(`/api/v1/users/${user_seed_1.id}/card-lists/${card_list_seed_4.id}`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_1)}`)
            .expect(HTTP_CODE.FORBIDDEN);

    });

});
