'use strict';

const qs = require('qs');
const superTest = require('supertest');
const HTTP_CODE = require('http-status');
const {
    expect,
} = require('chai');
const {
    app,
} = require('../../../src/app');

const {
    user_seed_1,
    user_seed_2,
} = require('../../test_seeds/collection_user_card_seeds');

const {
    DataSeeder,
} = require('../../test_helpers/DataSeeder');

const superApp = superTest(app);

const card_list_seed_1 = {
    id: 50,
    name: 'First test type 2',
    type_id: 2,
    user_id: user_seed_1.id,
};

const card_list_seed_3 = {
    id: 75,
    name: 'First Belonds to user 2',
    type_id: 2,
    user_id: user_seed_2.id,
};

const card_list_seed_4 = {
    id: 100,
    name: 'Second Belonds to user 2',
    type_id: 3,
    user_id: user_seed_2.id,
};

// Araignée sentinelle - fr - set cmr
const card_list_item_seed_1 = {
    card_list_id: card_list_seed_1.id,
    oracle_id: 'a37c9b13-dc45-4b55-a63d-5314abde7aab',
    card_id: '41e0f8ba-75ab-48ef-8680-7977563b8205',
    state: 3,
};

// Araignée sentinelle - fr - set cmr
const card_list_item_seed_2 = {
    card_list_id: card_list_seed_4.id,
    oracle_id: 'a37c9b13-dc45-4b55-a63d-5314abde7aab',
    card_id: '41e0f8ba-75ab-48ef-8680-7977563b8205',
    state: 1,
};

// Araignée sentinelle - fr - set m13
const card_list_item_seed_3 = {
    card_list_id: card_list_seed_4.id,
    oracle_id: 'a37c9b13-dc45-4b55-a63d-5314abde7aab',
    card_id: '6c0c074d-2e07-4bc9-97c6-250f972c44c0',
    state: 1,
};

// Araignée sentinelle - fr - set m13 Double
const card_list_item_seed_4 = {
    card_list_id: card_list_seed_4.id,
    oracle_id: 'a37c9b13-dc45-4b55-a63d-5314abde7aab',
    card_id: '6c0c074d-2e07-4bc9-97c6-250f972c44c0',
    state: 2,
};

// Fait ou fiction
const card_list_item_seed_5 = {
    card_list_id: card_list_seed_3.id,
    card_id: '015196c2-5151-4779-8e4d-9074713d3124',
    oracle_id: '437b2dab-15e0-4b9a-a204-58622d37a3b3',
    state: 2,
};

// Araignée sentinelle - fr - set cmr
const card_list_item_seed_6 = {
    card_list_id: card_list_seed_3.id,
    oracle_id: 'a37c9b13-dc45-4b55-a63d-5314abde7aab',
    card_id: '41e0f8ba-75ab-48ef-8680-7977563b8205',
    state: 1,
};


describe('SearchCardListItem functional test', () => {

    before(async () => {
        await DataSeeder.truncate('UserRepository');
        await DataSeeder.truncate('CardListRepository');
        await DataSeeder.truncate('CardListItemRepository');

        await DataSeeder.createUserHashPassword(user_seed_1);
        await DataSeeder.createUserHashPassword(user_seed_2);

        await DataSeeder.create('CardListRepository', card_list_seed_1);
        await DataSeeder.create('CardListRepository', card_list_seed_3);
        await DataSeeder.create('CardListRepository', card_list_seed_4);
        await DataSeeder.create('CardListItemRepository', card_list_item_seed_1);
        await DataSeeder.create('CardListItemRepository', card_list_item_seed_2);
        await DataSeeder.create('CardListItemRepository', card_list_item_seed_3);
        await DataSeeder.create('CardListItemRepository', card_list_item_seed_4);
        await DataSeeder.create('CardListItemRepository', card_list_item_seed_5);
        await DataSeeder.create('CardListItemRepository', card_list_item_seed_6);

    });


    it('Should be able to search by card_id', async () => {
        await superApp
            .get(`/api/v1/users/${user_seed_2.id}/card-list-items`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_2)}`)
            .query(qs.stringify({
                card_id: card_list_item_seed_6.card_id,
            }))
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('card_list_item_list');
                const {
                    card_list_item_list,
                } = response.body;
                expect(card_list_item_list).to.be.an('Array').and.have.lengthOf(2);


                // console.log(card_list_item_list);

                card_list_item_list.forEach((card_list_item) => {
                    expect(card_list_item).to.have.property('card_id', card_list_item_seed_6.card_id);
                    expect(card_list_item).to.have.property('id');
                });

            });
    });


    it('Should be able to search by card_id and card_list_type_id', async () => {
        await superApp
            .get(`/api/v1/users/${user_seed_2.id}/card-list-items`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_2)}`)
            .query(qs.stringify({
                card_id: card_list_item_seed_6.card_id,
                card_list_type_id: card_list_seed_3.type_id,
            }))
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('card_list_item_list');
                const {
                    card_list_item_list,
                } = response.body;
                expect(card_list_item_list).to.be.an('Array').and.have.lengthOf(1);

                card_list_item_list.forEach((card_list_item) => {
                    expect(card_list_item).to.have.property('card_id', card_list_item_seed_6.card_id);
                    expect(card_list_item).to.have.property('card_list_id', card_list_seed_3.id);
                });

            });
    });

    it('Should be able to search without any params', async () => {
        await superApp
            .get(`/api/v1/users/${user_seed_2.id}/card-list-items`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_2)}`)
            .query(qs.stringify({}))
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('card_list_item_list');
                const {
                    card_list_item_list,
                } = response.body;

                expect(card_list_item_list).to.be.an('Array').and.have.lengthOf(5);

            });
    });

    it('Should be able to search card_list_id param', async () => {
        await superApp
            .get(`/api/v1/users/${user_seed_2.id}/card-list-items`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_2)}`)
            .query(qs.stringify({
                card_list_id: card_list_seed_4.id,
            }))
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('card_list_item_list');
                const {
                    card_list_item_list,
                } = response.body;

                expect(card_list_item_list).to.be.an('Array').and.have.lengthOf(3);
                card_list_item_list.forEach((card_list_item) => {
                    expect(card_list_item).to.have.property('card_list_id', card_list_seed_4.id);
                });
            });
    });

    it('Should be able to search card_list_id_list any param', async () => {
        await superApp
            .get(`/api/v1/users/${user_seed_2.id}/card-list-items`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_2)}`)
            .query(qs.stringify({
                card_list_id_list: [
                    card_list_seed_4.id,
                ],
            }))
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('card_list_item_list');
                const {
                    card_list_item_list,
                } = response.body;

                expect(card_list_item_list).to.be.an('Array').and.have.lengthOf(3);
                card_list_item_list.forEach((card_list_item) => {
                    expect(card_list_item).to.have.property('card_list_id', card_list_seed_4.id);
                });
            });
    });

});
