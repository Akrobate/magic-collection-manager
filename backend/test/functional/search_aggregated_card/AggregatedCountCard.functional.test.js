/* eslint-disable func-names */

'use strict';

const qs = require('qs');
const superTest = require('supertest');
const HTTP_CODE = require('http-status');
const {
    expect,
} = require('chai');
const {
    app,
} = require('../../../src/app');

const {
    user_seed_1,
} = require('../../test_seeds/collection_user_card_seeds');

const {
    deck_red_blue,
} = require('../../test_seeds/decks');

const {
    DataSeeder,
} = require('../../test_helpers/DataSeeder');

const superApp = superTest(app);

const collection_list_seed = {
    id: 1,
    name: null,
    user_id: user_seed_1.id,
    type_id: 1,
};

describe('CollectionUserCardCount', function() {

    // eslint-disable-next-line no-invalid-this
    this.timeout(500000);

    before(async () => {
        await DataSeeder.truncate('UserRepository');
        await DataSeeder.truncate('CardListItemRepository');
        await DataSeeder.truncate('CardListRepository');

        await DataSeeder.createUserHashPassword(user_seed_1);

        await DataSeeder.create('CardListRepository', collection_list_seed);

        await DataSeeder.loadDecksSeedInCardListItemRepository(
            deck_red_blue,
            collection_list_seed.id
        );

    });


    it('Aggregated count card in list without criteria (limit 10 should not affect result)', async () => {
        await superApp
            .get(`/api/v1/users/${user_seed_1.id}/aggregated/count`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_1)}`)
            .query(qs.stringify({
                card_list_id: collection_list_seed.id,
                limit: 10,
            }))
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('card_count', 60);
            });
    });


    it('Aggregated count card in list (collection) with group by card_id', async () => {
        await superApp
            .get(`/api/v1/users/${user_seed_1.id}/aggregated/count`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_1)}`)
            .query(qs.stringify({
                card_list_id: collection_list_seed.id,
                group_by_card_id: true,
            }))
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('card_count', 21);
            });
    });


    it('Aggregated count card in collection with group by oracle_id', async () => {
        await superApp
            .get(`/api/v1/users/${user_seed_1.id}/aggregated/count`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_1)}`)
            .query(qs.stringify({
                group_by_oracle_id: true,
                card_list_id: collection_list_seed.id,
            }))
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('card_count', 17);
            });
    });


    it('Aggregated count card in collection width color_identity_list Blue (U)', async () => {
        await superApp
            .get(`/api/v1/users/${user_seed_1.id}/aggregated/count`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_1)}`)
            .query(qs.stringify({
                color_identity_list: ['U'],
                card_list_id: collection_list_seed.id,
            }))
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('card_count', 24);
            });
    });


    it('Aggregated count card in list width color_identity_list Red (G) Should not find', async () => {
        await superApp
            .get(`/api/v1/users/${user_seed_1.id}/aggregated/count`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_1)}`)
            .query(qs.stringify({
                color_identity_list: ['G'],
                card_list_id: collection_list_seed.id,
            }))
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('card_count', 0);
            });
    });

});
