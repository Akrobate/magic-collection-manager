/* eslint-disable func-names */

'use strict';

const qs = require('qs');
const superTest = require('supertest');
const HTTP_CODE = require('http-status');
const {
    expect,
} = require('chai');
const {
    app,
} = require('../../../src/app');

const {
    user_seed_1,
} = require('../../test_seeds/collection_user_card_seeds');

const {
    deck_green,
    // deck_red_blue,
} = require('../../test_seeds/decks');

const {
    DataSeeder,
} = require('../../test_helpers/DataSeeder');

const superApp = superTest(app);

describe('CollectionUserCardSearch', function() {

    // eslint-disable-next-line no-invalid-this
    this.timeout(500000);

    before(async () => {
        await DataSeeder.truncate('UserRepository');
        await DataSeeder.truncate('CardListItemRepository');
        await DataSeeder.truncate('CardListRepository');

        await DataSeeder.createUserHashPassword(user_seed_1);
        const collection_list_seed = {
            id: 1,
            name: null,
            user_id: user_seed_1.id,
            type_id: 1,
        };
        await DataSeeder.create('CardListRepository', collection_list_seed);

        await DataSeeder.loadDecksSeedInCardListItemRepository(deck_green, collection_list_seed.id);

    });


    it('Aggregated search card in collection without criteria (limit 10)', async () => {
        await superApp
            .get(`/api/v1/users/${user_seed_1.id}/aggregated/collection`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_1)}`)
            .query(qs.stringify({
                limit: 10,
            }))
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('card_list');
                const {
                    card_list,
                } = response.body;

                // console.log("card_list.length", card_list.length)

                card_list.forEach((card) => {
                    expect(card).to.have.property('printed_name');
                });
                expect(card_list).to.be.an('Array').and.to.have.lengthOf(10);
            });
    });


    it('Aggregated search card in collection with group by card_id and sort by count (limit 60)', async () => {
        await superApp
            .get(`/api/v1/users/${user_seed_1.id}/aggregated/collection`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_1)}`)
            .query(qs.stringify({
                limit: 60,
                group_by_card_id: true,
            }))
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('card_list');
                const {
                    card_list,
                } = response.body;

                // console.log("card_list.length", card_list.length)

                card_list.forEach((card) => {
                    expect(card).to.have.property('printed_name');
                });
                expect(card_list).to.be.an('Array').and.to.have.lengthOf(18);
            });
    });


    it('Aggregated search card in collection with group by oracle_id and sort by count (limit 60)', async () => {
        await superApp
            .get(`/api/v1/users/${user_seed_1.id}/aggregated/collection`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_1)}`)
            .query(qs.stringify({
                limit: 60,
                group_by_oracle_id: true,
            }))
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('card_list');
                const {
                    card_list,
                } = response.body;

                // console.log("card_list.length", card_list.length)

                card_list.forEach((card) => {
                    expect(card).to.have.property('printed_name');
                });
                expect(card_list).to.be.an('Array').and.to.have.lengthOf(17);
            });
    });


    it('Aggregated search card in collection with lang, group_by_oracle, desc count sort (limit 10)', async () => {
        await superApp
            .get(`/api/v1/users/${user_seed_1.id}/aggregated/collection`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_1)}`)
            .query(qs.stringify({
                limit: 10,
                offset: 0,
                lang: 'fr',
                group_by_oracle_id: true,
                sort: '-count',
            }))
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('card_list');
                const {
                    card_list,
                } = response.body;

                let previous_count = null;
                card_list.forEach((card) => {
                    // console.log('card_list', `${card.printed_name} ${card.count}`);
                    if (previous_count === null) {
                        previous_count = card.count;
                    } else {
                        expect(card.count).to.be.lte(previous_count);
                        previous_count = card.count;
                    }
                    expect(card).to.have.property('printed_name');
                });

                expect(card_list).to.be.an('Array').and.to.have.lengthOf(10);
            });
    });


    it('Aggregated search card in collection with lang (limit 10)', async () => {
        await superApp
            .get(`/api/v1/users/${user_seed_1.id}/aggregated/collection`)
            .set('Authorization', `Bearer ${DataSeeder.getJwtToken(user_seed_1)}`)
            .query(qs.stringify({
                limit: 10,
                offset: 0,
                group_by_oracle_id: true,
                sort: 'printed_name',
            }))
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('card_list');
                const {
                    card_list,
                } = response.body;

                let previous_printed_name = null;
                card_list.forEach((card) => {
                    expect(card).to.have.property('printed_name');
                    // console.log('card_list', `${card.printed_name} ${card.count}`);
                    if (previous_printed_name === null) {
                        previous_printed_name = card.printed_name;
                    } else {
                        expect(card.printed_name.charCodeAt(0))
                            .to.be.gte(previous_printed_name.charCodeAt(0));
                        previous_printed_name = card.printed_name;
                    }
                });

                expect(card_list).to.be.an('Array').and.to.have.lengthOf(10);
            });
    });


});
