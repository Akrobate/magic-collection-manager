'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');

const {
    expect,
} = require('chai');
const {
    mock,
} = require('sinon');
const {
    app,
} = require('../../../src/app');
const {
    CardReferentialService,
} = require('../../../src/services');
const {
    JsonFileRepository,
} = require('../../../src/repositories');


const superApp = superTest(app);
const card_referential_service = CardReferentialService.getInstance();

describe('CardReferential functional test', () => {

    const mocks = {};

    beforeEach(() => {
        card_referential_service.setDataSourceBasePath(`${__dirname}/../../test_seeds/card_referentials/`);
        const json_file_repository = JsonFileRepository.getInstance();
        mocks.json_file_repository = mock(json_file_repository);
    });

    afterEach(() => {
        card_referential_service.color_list = null;
        card_referential_service.set_list = null;
        mocks.json_file_repository.restore();
    });

    it('Should be able to get card referentials colors', async () => {
        await superApp
            .get('/api/v1/cards/referentials/colors')
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('color_list');
                expect(response.body.color_list).to.be.an('Array')
                    .and.to.have.lengthOf(6);

                response.body.color_list.forEach((color) => {
                    expect(color).to.have.property('text');
                    expect(color).to.have.property('color');
                    expect(color).to.have.property('symbol');
                    expect(color).to.have.property('svg_uri');
                });
            });
    });

    it('Should be able to get card colors referentials cache', async () => {

        mocks.json_file_repository
            .expects('getData')
            .withArgs(`${__dirname}/../../test_seeds/card_referentials/card-symbol-referential.json`)
            .once()
            .returns(Promise.resolve({
                data: [],
            }));

        await superApp
            .get('/api/v1/cards/referentials/colors')
            .expect(HTTP_CODE.OK);

        await superApp
            .get('/api/v1/cards/referentials/colors')
            .expect(HTTP_CODE.OK);

        await superApp
            .get('/api/v1/cards/referentials/colors')
            .expect(HTTP_CODE.OK);
        mocks.json_file_repository.verify();
    });

    it('Should be able to get card referentials sets', async () => {
        await superApp
            .get('/api/v1/cards/referentials/sets')
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('set_list');
                expect(response.body.set_list).to.be.an('Array');
                response.body.set_list.forEach((set) => {
                    expect(set).to.have.property('id');
                    expect(set).to.have.property('code');
                    expect(set).to.have.property('name');
                    expect(set).to.have.property('released_at');
                    expect(set).to.have.property('set_type');
                    expect(set).to.have.property('card_count');
                    expect(set).to.have.property('parent_set_code');
                    expect(set).to.have.property('icon_svg_uri');
                });
            });
    });

    it('Should be able to get card sets referentials cache', async () => {

        mocks.json_file_repository
            .expects('getData')
            .withArgs(`${__dirname}/../../test_seeds/card_referentials/card-set-referential.json`)
            .once()
            .returns(Promise.resolve({
                data: [],
            }));

        await superApp
            .get('/api/v1/cards/referentials/sets')
            .expect(HTTP_CODE.OK);

        await superApp
            .get('/api/v1/cards/referentials/sets')
            .expect(HTTP_CODE.OK);

        await superApp
            .get('/api/v1/cards/referentials/sets')
            .expect(HTTP_CODE.OK);
        mocks.json_file_repository.verify();
    });

});
