'use strict';

const process = require('process');
const {
    stub,
} = require('sinon');

const {
    MongoDbRepository,
    JsonFileRepository,
} = require('../src/repositories');


const MUTE_CONSOLE_LOGS = true;
const stubs = {};


const file_path = 'test/test_seeds/mongodb_cards_seeds/all_french_green_cards.json';

before(async () => {
    if (MUTE_CONSOLE_LOGS) {
        stubs.console_log = stub(console, 'log').callsFake(() => null);
    }
    const mongo_db_repository = await MongoDbRepository.getInstance();
    await mongo_db_repository.dropDatabase();
    const seed_array = await JsonFileRepository.getInstance().getData(`${process.cwd()}/${file_path}`);
    await mongo_db_repository.insertDocumentList('cards', seed_array);
    console.log('............... Before all  ..............');
    console.log(` * Loaded ${seed_array.length} mongodb cards`);

});
