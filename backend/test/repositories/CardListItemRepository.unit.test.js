'use strict';

const {
    expect,
} = require('chai');
const {
    CardListItemRepository,
} = require('../../src/repositories');

const {
    collection_user_card_seed_6,
    collection_user_card_seed_7,
    collection_user_card_seed_8,
    collection_user_card_seed_9,
    collection_user_card_seed_10,
} = require('../test_seeds/collection_user_card_seeds');

const {
    DataSeeder,
} = require('../test_helpers/DataSeeder');

describe('CollectionUserCardSearch', () => {

    const card_list_item_repository = CardListItemRepository.getInstance();

    before(async () => {
        await DataSeeder.truncate('CardListItemRepository');
        await DataSeeder.create('CardListItemRepository', collection_user_card_seed_6);
        await DataSeeder.create('CardListItemRepository', collection_user_card_seed_7);
        await DataSeeder.create('CardListItemRepository', collection_user_card_seed_8);
        await DataSeeder.create('CardListItemRepository', collection_user_card_seed_9);
        await DataSeeder.create('CardListItemRepository', collection_user_card_seed_10);
    });

    it('Should be able to search by card_list_id', async () => {
        const result = await card_list_item_repository.search({
            card_list_id: collection_user_card_seed_6.card_list_id,
        });
        expect(result).to.be.an('Array').and.have.lengthOf(5);
    });


    it('Should be able to search by card_list_id and group by card_id', async () => {
        const result = await card_list_item_repository.search({
            card_list_id: collection_user_card_seed_6.card_list_id,
            group_by_card_id: true,
            sort_by_count: 'desc',
        });
        // console.log(result);
        expect(result).to.be.an('Array').and.have.lengthOf(4);
    });


    it('Should be able to search by card_list_id and group by oracle_id', async () => {
        const result = await card_list_item_repository.search({
            card_list_id: collection_user_card_seed_6.card_list_id,
            group_by_oracle_id: true,
            sort_by_count: 'asc',
        });
        // console.log(result);
        expect(result).to.be.an('Array').and.have.lengthOf(3);
    });

    it('Should be able to search by card_list_id and group by oracle_id and count_upper_boundary filter', async () => {
        const result = await card_list_item_repository.search({
            card_list_id: collection_user_card_seed_6.card_list_id,
            group_by_oracle_id: true,
            sort_by_count: 'asc',
            // count_lower_boundary: 1,
            count_upper_boundary: 1,
        });
        // console.log(result);
        expect(result).to.be.an('Array').and.have.lengthOf(1);
    });

    it('Should be able to search by card_list_id and group by oracle_id and count_lower_boundary filter', async () => {
        const result = await card_list_item_repository.search({
            card_list_id: collection_user_card_seed_6.card_list_id,
            group_by_oracle_id: true,
            sort_by_count: 'asc',
            count_lower_boundary: 1,
            // count_upper_boundary: 1,
        });
        // console.log(result);
        expect(result).to.be.an('Array').and.have.lengthOf(3);
    });

});
