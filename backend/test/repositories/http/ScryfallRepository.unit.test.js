'use strict';

const {
    expect,
} = require('chai');
const {
    ScryfallRepository,
} = require('../../../src/repositories/http/');
const {
    mock,
} = require('sinon');

const axios = require('axios');

describe('ScryfallRepository', () => {

    const mocks = {};
    const scryfall_repository = ScryfallRepository.getInstance();

    beforeEach(() => {
        mocks.axios = mock(axios);
    });

    afterEach(() => {
        mocks.axios.restore();
    });

    it('Should be able to get getCardSymbols list', async () => {
        mocks.axios.expects('get')
            .withArgs('https://api.scryfall.com/symbology')
            .once()
            .returns(Promise.resolve({
                data: [
                    {
                        object: 'card_symbol',
                        symbol: '{C}',
                        svg_uri: 'https://c2.scryfall.com/file/scryfall-symbols/card-symbols/C.svg',
                        loose_variant: 'C',
                        english: 'one colorless mana',
                        transposable: false,
                        represents_mana: true,
                        appears_in_mana_costs: true,
                        cmc: 1,
                        funny: false,
                        colors: [],
                        gatherer_alternates: null,
                    },
                ],
            }));

        const result = await scryfall_repository.getCardSymbols();

        expect(result).to.be.an('Array').and.have.lengthOf(1);
        const [
            first,
        ] = result;
        expect(first).to.have.property('object', 'card_symbol');
        expect(first).to.have.property('symbol', '{C}');
        expect(first).to.have.property('svg_uri', 'https://c2.scryfall.com/file/scryfall-symbols/card-symbols/C.svg');
    });


    it('Should be able to get getBulkData list', async () => {

        mocks.axios.expects('get')
            .withArgs('https://api.scryfall.com/bulk-data')
            .once()
            .returns(Promise.resolve({
                data: [
                    {
                        object: 'bulk_data',
                        id: '922288cb-4bef-45e1-bb30-0c2bd3d3534f',
                        type: 'all_cards',
                        updated_at: '2021-10-20T21:12:55.973+00:00',
                        uri: 'https://api.scryfall.com/bulk-data/922288cb-4bef-45e1-bb30-0c2bd3d3534f',
                        name: 'All Cards',
                        description: 'A JSON file containing every card object on Scryfall in every language.',
                        compressed_size: 207747420,
                        download_uri: 'https://c2.scryfall.com/file/scryfall-bulk/all-cards/all-cards-20211020211255.json',
                        content_type: 'application/json',
                        content_encoding: 'gzip',
                    },

                ],
            }));

        const result = await scryfall_repository.getBulkData();

        expect(result).to.be.an('Array').and.have.lengthOf(1);
        const [
            first,
        ] = result;
        expect(first).to.have.property('object', 'bulk_data');
        expect(first).to.have.property('download_uri');
        expect(first).to.have.property('id', '922288cb-4bef-45e1-bb30-0c2bd3d3534f');
    });


    it('Should be able to get getCardSets list', async () => {

        mocks.axios.expects('get')
            .withArgs('https://api.scryfall.com/sets')
            .once()
            .returns(Promise.resolve({
                data: [
                    {
                        object: 'set',
                        id: '43057fad-b1c1-437f-bc48-0045bce6d8c9',
                        code: 'khm',
                        mtgo_code: 'khm',
                        arena_code: 'khm',
                        tcgplayer_id: 2750,
                        name: 'Kaldheim',
                        uri: 'https://api.scryfall.com/sets/43057fad-b1c1-437f-bc48-0045bce6d8c9',
                        scryfall_uri: 'https://scryfall.com/sets/khm',
                        search_uri: 'https://api.scryfall.com/cards/search?order=set&q=e%3Akhm&unique=prints',
                        released_at: '2021-02-05',
                        set_type: 'expansion',
                        card_count: 405,
                        digital: false,
                        nonfoil_only: false,
                        foil_only: false,
                        icon_svg_uri: 'https://c2.scryfall.com/file/scryfall-symbols/sets/khm.svg?1634529600',
                    },
                ],
            }));

        const result = await scryfall_repository.getCardSets();

        expect(result).to.be.an('Array').and.have.lengthOf(1);
        const [
            first,
        ] = result;
        expect(first).to.have.property('code', 'khm');
        expect(first).to.have.property('id', '43057fad-b1c1-437f-bc48-0045bce6d8c9');
        expect(first).to.have.property('name', 'Kaldheim');
    });

});
