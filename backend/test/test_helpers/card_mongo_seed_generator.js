'use strict';

const process = require('process');


const {
    MongoDbRepository,
    JsonFileRepository,
} = require('../../src/repositories');

const mongo_db_repository = MongoDbRepository.getInstance();
const json_file_repository = JsonFileRepository.getInstance();

const file_path = 'test/test_seeds/mongodb_cards_seeds/all_french_green_cards.json';

const sets = [
    // starter kit green deck
    'm21',
    'iko',
    'thb',
    'eld',

    // starter kit red blue deck
    'khm',
    'znr',
    'afr',
    'stx',
];

const card_seed_criteria = {
    lang: 'fr',
    set: {
        $in: sets,
    },
};

const source_collection_name = 'cards';

async function generate() {
    const results = await mongo_db_repository
        .findDocumentList(source_collection_name, card_seed_criteria);
    console.log(results.length);
    await json_file_repository.saveData(`${process.cwd()}/${file_path}`, results);
}

(async () => {
    console.log('Quering seeds');
    await generate();

    await mongo_db_repository.closeConnection();
})();
