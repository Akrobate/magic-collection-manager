'use strict';

const {
    expect,
} = require('chai');

const {
    CardValuesPrecalculatorService,
} = require('../../../src/services');


describe('CardValuesPrecalculatorService', () => {

    const card_values_precalculator_service = CardValuesPrecalculatorService.getInstance();

    it('generateOneType nomination case', (done) => {
        const seed = 'Creature — Sliver';
        const result = card_values_precalculator_service.generateOneType(seed);
        expect(result).to.deep.equal(
            {
                type: 'creature',
                sub_type: 'sliver',
            }
        );
        done();
    });


    it('generateOneType no subtype', (done) => {
        const seed = 'Creature ';
        const result = card_values_precalculator_service.generateOneType(seed);
        expect(result).to.deep.equal(
            {
                type: 'creature',
                sub_type: null,
            }
        );
        done();
    });


    it('generateTypeObject nominal case', (done) => {
        const seed = 'Creature — Sliver';
        const result = card_values_precalculator_service.generateTypeObject(seed);
        expect(result).to.deep.equal(
            {
                type_1: 'creature',
                sub_type_1: 'sliver',
                type_2: null,
                sub_type_2: null,
            }
        );
        done();
    });


    it('generateTypeObject nominal case with double face card', (done) => {
        const seed = 'Creature — Sliver // Creature2 — Sliver2';
        const result = card_values_precalculator_service.generateTypeObject(seed);
        expect(result).to.deep.equal(
            {
                type_1: 'creature',
                sub_type_1: 'sliver',
                type_2: 'creature2',
                sub_type_2: 'sliver2',
            }
        );
        done();
    });


    it('generateColorIdentityString nominal', (done) => {
        const seed = ['B', 'G'];
        const result = card_values_precalculator_service.generateColorIdentityString(seed);
        expect(result).to.equal('B_G');
        done();
    });


    it('generateColorIdentityString nominal', (done) => {
        const seed = [];
        const result = card_values_precalculator_service.generateColorIdentityString(seed);
        expect(result).to.equal('');
        done();
    });


    it('generatePtcScore nominal', (done) => {
        const result = card_values_precalculator_service.generatePtcScore({
            power: '3',
            toughness: '3',
            cmc: '6',
        });
        expect(result).to.equal(1);
        done();
    });


    it('generatePtcScore nominal', (done) => {
        const result = card_values_precalculator_service.generatePtcScore({
            power: '10',
            toughness: '10',
            cmc: '5',
        });
        expect(result).to.equal(4);
        done();
    });


});
