'use strict';

const Promise = require('bluebird');

const {
    MongoDbRepository,
} = require('../src/repositories/MongoDbRepository');
const {
    sequelize,
} = require('../src/repositories/connections/Sequelize');

after(async () => {
    await Promise.delay(500);
    const mongo_db_repository = MongoDbRepository.getInstance();
    await mongo_db_repository.closeConnection();
    await sequelize.close();
    console.log('............... After all Teardown, closing mongoDb connection ..............');
});
