'use strict';

/**
 * Starter kit green / black
 * deck Green
 */

const card_list_id = 100;
const state = 1;

const deck = [
    {
        // Arni Frontcassé
        card_id: '7bc44100-d992-444d-9784-2630489f4cf6',
        oracle_id: '3dce6c6c-599f-4e35-ae15-728c5e78dee1',
        count: 1,
    },
    {
        // Porteur de calamité
        card_id: 'f86c3b53-1a3d-46d7-9574-6bec60d292d8',
        oracle_id: 'b3f7c4ac-b788-4307-9bc3-d99ce310e07c',
        count: 1,
    },
    {
        // Cavalerie axgardienne
        card_id: '8bcc0898-da5c-4948-8bc8-396914236419',
        oracle_id: '6a92b011-fd01-4b6a-a17c-73ac1967ff98',
        count: 4,
    },
    {
        // Embusqueur des Pics vacillants
        card_id: 'e992ca64-a1d6-4770-a4cd-3e09a85f4177',
        oracle_id: 'fe769071-3f99-411b-878a-57a2200649a0',
        count: 4,
    },
    {
        // Dragon rouge
        card_id: '0321cc9e-fdd8-489c-abcb-b0a73508f84c',
        oracle_id: 'ff6e4346-463c-445d-8f72-11cb35dd99ee',
        count: 3,
    },
    {
        // Archimage émérite
        card_id: '93a97c61-9702-40ea-a8d7-c446f5239b39',
        oracle_id: '8305d576-21d8-4ce7-8eda-a7cd9793aca5',
        count: 1,
    },
    {
        // Invocateur de cyclone
        card_id: 'c50ace1c-0760-4ee4-9514-ec526dc083be',
        oracle_id: '2088e791-9ce5-4e97-a4ce-f51adaa3f42b',
        count: 1,
    },
    {
        // Flagelleur mental
        card_id: '781c7ed0-ab5d-47d0-8a8d-52f1a87c7374',
        oracle_id: '8dd473c7-afd9-40f1-bb8e-0a00f4687318',
        count: 1,
    },
    {
        // Serpent trou-de-ver
        card_id: 'aa750798-43bc-45da-a0a7-b9091fe1f1f5',
        oracle_id: '6eb21aca-116e-427b-9e75-8adaee0cc62a',
        count: 2,
    },
    {
        // Coureuse de vortex
        card_id: '2cad39e1-1ffd-4bc5-aac0-e93783057cd6',
        oracle_id: '37cf4ee6-e146-4354-afc8-6b59b0700e87',
        count: 2,
    },
    {
        // Escroc de givre
        card_id: '2aac5e27-61b7-460a-b3d4-24c72edefd50',
        oracle_id: '1663d54f-4f39-4a03-aac2-e2923cbb7de8',
        count: 4,
    },
    {
        // Recherches sur le terrain
        card_id: 'debf4212-ccb3-41df-944c-3f051440e6e1',
        oracle_id: 'bb377cf8-29ee-489e-86a9-93f4dc2ae02e',
        count: 2,
    },
    {
        // Blâme fulminant
        card_id: 'e09d1bd4-bfd5-41ca-bb78-3a98ff72c02d',
        oracle_id: 'bb4d9770-f230-4413-a7a4-bb1f1303661a',
        count: 3,
    },
    {
        // Folie furieuse
        card_id: '193b96fc-6c44-46fd-985d-07be117a2094',
        oracle_id: 'd463de75-8d3c-4077-a2cc-c304a63acc69',
        count: 4,
    },
    {
        // Étendues sauvages en évolution
        card_id: 'fc435ecb-4c9b-4ada-a88c-3e4709b4764c',
        oracle_id: 'a75445d3-1303-4bb5-89ad-26ea93fecd48',
        count: 4,
    },
    {
        // Montagne
        card_id: '8f8e183e-20f9-4baf-b054-b5a3082c687e',
        oracle_id: 'a3fb7228-e76b-4e96-a40e-20b5fed75685',
        count: 4,
    },
    {
        // Montagne
        card_id: '358022d5-12aa-433c-8835-7dcccbceba90',
        oracle_id: 'a3fb7228-e76b-4e96-a40e-20b5fed75685',
        count: 4,
    },
    {
        // Montagne
        card_id: '95d04448-88dc-414a-a8ef-d1e44658eee6',
        oracle_id: 'a3fb7228-e76b-4e96-a40e-20b5fed75685',
        count: 4,
    },
    {
        // Île
        card_id: '675c263f-0f60-4a0c-9e39-e5484bd623c1',
        oracle_id: 'b2c6aa39-2d2a-459c-a555-fb48ba993373',
        count: 4,
    },
    {
        // Île
        card_id: 'b4650d52-ec22-4de4-a464-0578bea5767f',
        oracle_id: 'b2c6aa39-2d2a-459c-a555-fb48ba993373',
        count: 3,
    },
    {
        // Île
        card_id: 'ce8c3f40-6a80-4342-8fb9-98fa97a2fd16',
        oracle_id: 'b2c6aa39-2d2a-459c-a555-fb48ba993373',
        count: 4,
    },
];

module.exports = {
    deck,
    card_list_id,
    state,
};
