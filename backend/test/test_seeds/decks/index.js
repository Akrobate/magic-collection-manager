'use strict';

const deck_green = require('./deck_green');
const deck_red_blue = require('./deck_red_blue');

module.exports = {
    deck_green,
    deck_red_blue,
};
