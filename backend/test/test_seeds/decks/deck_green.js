'use strict';

/**
 * Starter kit green / black
 * deck Green
 */

const card_list_id = 100;
const state = 1;

const deck = [
    {
        // Yorvo, seigneur de Garenbrig
        card_id: '90a2c6e6-b2b9-4429-954b-21f75ddd2f31',
        oracle_id: '00a15abc-bdac-4977-90a0-f08d41941d01',
        count: 1,
    },
    {
        // Chimère tremblebois
        card_id: '63924ddb-74eb-480d-8fb9-1d68f2b6abb0',
        oracle_id: '8b119b69-4794-443e-aa13-747c125b5b1b',
        count: 1,
    },
    {
        // Kogla, le singe titan
        card_id: 'aa754702-9607-49c2-9a45-05d1ae8c9c40',
        oracle_id: '8b21062e-97b0-4967-96df-30e8309f4fba',
        count: 1,
    },
    {
        // Hydre à écailles de fer
        card_id: '1cb2dc70-2cfd-48e6-8d7a-ff544bf21a51',
        oracle_id: 'cef72b9b-91b1-47ff-ab6e-91d3c548e98b',
        count: 1,
    },
    {
        // Fileuse de collet
        card_id: '7ed391df-49f4-41ba-93e5-1be3fcccd55a',
        oracle_id: '0f63de7b-03f2-49ad-b217-88fe9c50d68a',
        count: 2,
    },
    {
        // Gueuleffroi colossal
        card_id: '23ac7397-4c14-4a70-9b37-29dbc118dff8',
        oracle_id: '08c7db90-c0cf-4482-b7ee-bb033e5996d2',
        count: 2,
    },
    {
        // Mammouth à miel
        card_id: 'a68256a2-1ba7-42b0-a8ea-f13da7326ef6',
        oracle_id: '41da0e8c-ba86-446d-9719-667695c7f1cc',
        count: 2,
    },
    {
        // Humble naturaliste
        card_id: '5ad0406d-f816-4810-bdfe-601135f3dfe2',
        oracle_id: 'ff3a69a7-a244-4958-9e8b-d153942c1ba6',
        count: 2,
    },
    {
        // Sanglier hérissé
        card_id: 'b876b273-b3f9-4efe-bcd5-f2809251421f',
        oracle_id: 'f4aed3d2-04f5-49a4-a6be-d3cf097903f8',
        count: 3,
    },
    {
        // Visionnaire de Llanowar
        card_id: 'dd6902ad-b158-46aa-9824-161ac6d63868',
        oracle_id: 'f75ed312-3a23-4624-80c5-03980aa22d0b',
        count: 3,
    },
    {
        // Bosquérisson tout-puissant
        card_id: '4e839f89-f15f-4d61-aa34-6c37c4ca630f',
        oracle_id: 'd8a3a89d-9d27-40ae-a1aa-d55cc6a79b0b',
        count: 3,
    },
    {
        // Éclaireuse de la tour d'Hyrax
        card_id: '12585c0b-5cfa-4f8a-8929-fc1e1a7bded7',
        oracle_id: 'c1261a72-ccbd-4be9-bc3b-c1bcb807eb85',
        count: 3,
    },
    {
        // Coup de bélier
        card_id: '75a398b1-2f4d-4d70-94db-f14397ba618f',
        oracle_id: '4b1befff-48fa-47a3-832e-bdaf43495c02',
        count: 3,
    },
    {
        // Croissance titanesque
        card_id: '38bb57ca-dfa9-442a-a913-ec8428e73b1e',
        oracle_id: '61e09dd9-7870-48c2-9177-d6abc3162692',
        count: 4,
    },
    {
        // Colossification
        card_id: '63a66888-ec89-457e-b8e5-82b8e642cb9c',
        oracle_id: '05c5ee65-651b-4bfb-b57f-305026507135',
        count: 1,
    },
    {
        // Tomber à pic
        card_id: '61b776dd-30dc-43bc-b1cb-6653637b8cc4',
        oracle_id: '85bde6ac-3dd4-4946-8b57-24f57e3eae2b',
        count: 2,
    },
    {
        // Forêt
        card_id: 'ee7cfb04-35ba-4977-8370-f563a5ebfd64',
        oracle_id: 'b34bb2dc-c1af-4d77-b0b3-a0fb342a5fc6',
        count: 9,
    },
    {
        // Forêt
        card_id: 'b2d7f8bb-7db0-4f82-a724-4a1c266777dd',
        oracle_id: 'b34bb2dc-c1af-4d77-b0b3-a0fb342a5fc6',
        count: 9,
    },
    {
        // Forêt // collection number: 274
        card_id: 'b2d7f8bb-7db0-4f82-a724-4a1c266777dd',
        oracle_id: 'b34bb2dc-c1af-4d77-b0b3-a0fb342a5fc6',
        count: 8,
    },
];

module.exports = {
    deck,
    card_list_id,
    state,
};
