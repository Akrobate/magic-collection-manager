'use strict';

/**
 * Starter kit green / black
 * deck Green
 */

const user_id = 100;
const state = 1;

const card_list_items = [
    {
        // Onirophage
        card_id: '3d9d79d6-18ce-444b-8b3c-95d1f1ddb8d4',
        oracle_id: '48c97aeb-6591-4175-a24b-d3488bf3ab0b',
        count: 25,
    },
    {
        // Fait ou fiction
        card_id: '015196c2-5151-4779-8e4d-9074713d3124',
        oracle_id: '437b2dab-15e0-4b9a-a204-58622d37a3b3',
        count: 24,
    },
    {
        // Exclusion
        card_id: '24cb6f22-c00f-46da-8195-cf47b9afee24',
        oracle_id: 'a8c9f91a-b1e7-451d-b6db-ae865e2b853c',
        count: 21,
    },
    {
        // Remise à neuf
        card_id: '6ea126ca-dd77-4bb6-afe5-067a466367f5',
        oracle_id: '496999a7-a82a-45ea-9c4e-1b6aabc781cd',
        count: 26,
    },
    {
        // Slivoïde galopant
        card_id: 'c35216c9-92fa-4936-8e06-7a8ca93486a7',
        oracle_id: '7c3dcfea-f42c-4515-9b7d-6e9d8cb4d7f9',
        count: 22,
    },
    {
        // Mage aux hommages
        card_id: 'da5ab42e-e248-434e-a490-d9751ac7b886',
        oracle_id: 'c2b51295-8820-425f-a517-55231123c7de',
        count: 23,
    },
    {
        // Reflet perverti
        card_id: 'd99e023e-f32f-4767-9ce0-9295e653c799',
        oracle_id: '57fdf496-c31e-4a3b-a8a6-4f053b808267',
        count: 23,
    },
    {
        // Gardien de demain
        card_id: '44a59e7d-6ffa-450f-8777-30a3c62f5cd4',
        oracle_id: '277aafa6-4270-4d24-a888-cf9e8e635d4b',
        count: 8,
    },
    {
        // Piaillement de bataille
        card_id: '854e3cd0-4b0f-4693-9e15-a4d624e42a5d',
        oracle_id: 'e73131cd-b454-405b-9539-9d777e232b9e',
        count: 5,
    },
    {
        // Hélice de châtiment
        card_id: 'ba1307b8-bf41-417e-949a-39eaeea92433',
        oracle_id: '19d19e32-af13-4bb3-925f-8ed54cce2fcb',
        count: 19,
    },
    {
        // Traqueur de gorge
        card_id: 'ed02cee3-d8da-467c-a976-e12c07f05858',
        oracle_id: 'edb7bd88-971a-4fcc-9d76-d73f21a4926a',
        count: 27,
    },
    {
        // Augure mort-vivant
        card_id: '9f452907-3e3a-4f95-aabc-5312faa1c176',
        oracle_id: 'c26887e1-27f4-4550-921b-53460e43c079',
        count: 20,
    },
    {
        // Strix du blizzard
        card_id: '83225595-1e54-4533-a4e4-6f8ebf57a2b7',
        oracle_id: 'c687af9b-2159-4965-a068-ce4db1ce801c',
        count: 25,
    },
    {
        // Évasion rusée
        card_id: '86c64c49-4fff-4ae8-aed8-a81976fa5eab',
        oracle_id: '3e56606d-562e-4c77-9769-213997fea28e',
        count: 25,
    },
    {
        // Rêve éternel
        card_id: '79703a47-22ff-40a7-9857-447e1608a557',
        oracle_id: '5b17d3c5-6085-416e-9afd-8ed0639ae7d2',
        count: 27,
    },
    {
        // Coup désactivateur
        card_id: 'ee614721-6c3d-4ef6-b459-512000b5f01a',
        oracle_id: '300cba5a-adbb-4852-be06-ac00d9d6fd37',
        count: 14,
    },
    {
        // Visage de la divinité
        card_id: '6166dd27-7e56-459b-8c02-380313a59dff',
        oracle_id: '90b5ed05-7a67-4bd3-a8c4-2a4565d084fc',
        count: 23,
    },
    {
        // Slivoïde de la Bourbière
        card_id: '557ba08c-b6c4-4563-beb8-0a4221d6b03f',
        oracle_id: 'd1678eed-a335-454c-9fa1-749317500a36',
        count: 24,
    },
    {
        // Festoyeur d'imbéciles
        card_id: '37d7ff72-1afd-4fac-ab44-efec5300d485',
        oracle_id: '3e3d4f28-93a0-4313-a9a0-48fd06540cf8',
        count: 29,
    },
    {
        // Fauxsoyeur
        card_id: '2113f1f2-d26b-4a16-9f3d-c030b316e0ef',
        oracle_id: '1eebc02a-9a64-49b0-8322-c5ce671457ce',
        count: 29,
    },
    {
        // Obsession sadique
        card_id: '2266c705-513c-43c4-8973-ece2017f9f82',
        oracle_id: '1c55af05-7fbe-4078-9463-689b21c0d3e4',
        count: 28,
    },
    {
        // Suppositions fracassées
        card_id: 'be7be7b4-e77f-47f9-94b0-ccd57c98602e',
        oracle_id: 'c1372f18-9b33-40e8-bfe2-dc4ca3b3ef55',
        count: 26,
    },
    {
        // Lieutenant des catapultants
        card_id: 'e22af773-3ef7-4cb5-a345-aa4d035a745c',
        oracle_id: '4e93b23c-a7f7-4bdd-bbca-0dc48bb5c223',
        count: 25,
    },
    {
        // Pillage
        card_id: '2911c465-b4ce-40bb-ba52-f89adb764925',
        oracle_id: '0b137853-7cb9-424b-8285-12938991eafb',
        count: 24,
    },
    {
        // Géant vorace
        card_id: '599589d3-3749-43e4-a6af-eed11fa55f90',
        oracle_id: '46c7a552-4a98-4e1a-8651-8fb0e7153aec',
        count: 24,
    },
    {
        // Affres du chaos
        card_id: 'c49a801b-0c54-4952-8889-a22a62376e8b',
        oracle_id: 'e3444fcf-70ed-4d6e-aea9-030af15cad56',
        count: 24,
    },
    {
        // Rage selon Urza
        card_id: 'd989be7a-59af-4838-8869-c77c3d119a8e',
        oracle_id: '363f8c66-fe0c-44b9-987d-1d160e3f9c54',
        count: 26,
    },
    {
        // Diable vengeur
        card_id: '908b1a7e-c6b3-4088-9633-97e90e12b06f',
        oracle_id: 'ba771caa-2cfb-4373-bbd9-6160b0543010',
        count: 24,
    },
    {
        // Rats des cryptes
        card_id: 'b740a5df-d818-4c1d-af8c-2d60d5a8a873',
        oracle_id: '104095ed-55e3-408e-bf70-4fe06bb16d2f',
        count: 22,
    },
    {
        // Guide alpin
        card_id: '9c1a1ce8-42f1-4d62-a2c8-c1d77b35ad75',
        oracle_id: '55b12241-2495-45e6-b618-3746d5521291',
        count: 26,
    },
    {
        // Coup de flamme
        card_id: 'd886cb62-d469-4654-90dc-57cb9c7738a4',
        oracle_id: '7aa31280-12c3-479d-a6dd-f1c669043083',
        count: 25,
    },
    {
        // Oriflamme gobeline
        card_id: '35507f64-a505-43eb-9f35-7552a7cebdea',
        oracle_id: '836bd011-2da5-443a-a814-19a664b98a1a',
        count: 32,
    },
    {
        // Slivoïde à tête creuse
        card_id: '535ae78f-1f31-487d-b7d2-7c2ed7d288cb',
        oracle_id: '739ad367-a4ff-4779-8888-3c27f0f60253',
        count: 26,
    },
    {
        // Vigile aux écailles de minerai
        card_id: 'ec0d26c8-a22a-485e-bf50-9a36dd647b77',
        oracle_id: 'c4224ae5-4807-45cb-bd4b-70f2c0d9d9f4',
        count: 31,
    },
    {
        // Transporteur de braises
        card_id: '3f418a5d-2e35-4096-92cc-4c32e0b96835',
        oracle_id: '7d313958-49ca-4592-97c7-20f12c8468a8',
        count: 26,
    },
    {
        // Dragon mage
        card_id: '2955b9ec-48d9-4131-bb69-a92c1e664352',
        oracle_id: 'eab71e4e-27c3-4c41-b95f-259c1d14b97a',
        count: 22,
    },
    {
        // Fureur déchaînée
        card_id: 'adf136c1-de27-4c2a-b2c2-2d9235ed7f0d',
        oracle_id: '7aac51ef-ae03-4a9e-9a48-46223787f291',
        count: 18,
    },
    {
        // Dragon insatiable
        card_id: '25e2b816-fd3b-4504-bd20-7593ccee6112',
        oracle_id: '0944ec2e-1dd9-459f-8f1d-667242cf52fe',
        count: 21,
    },
    {
        // Crachefeu de Chandra
        card_id: '67eabb58-5942-4b1f-b271-489fc59d8f52',
        oracle_id: '5a0eb270-b142-45da-87a2-2f1c4e25db17',
        count: 19,
    },
    {
        // Chandra, pyromancienne novice
        card_id: '68580b9d-1a78-418f-a1a1-27097a0271b1',
        oracle_id: '882619f7-69b4-4ce0-be4e-1e34e608f925',
        count: 15,
    },
    {
        // Brûleur gambadant
        card_id: 'f1855feb-daa7-4cfd-a886-5fbc61323687',
        oracle_id: '4899595e-e626-432c-9c9f-f9d5c243a64b',
        count: 17,
    },
    {
        // Meneur gobelin
        card_id: 'aad55ac3-ca20-4cde-b7a2-352695491649',
        oracle_id: '4100e486-0d27-436c-8429-76bc2c1a26ab',
        count: 18,
    },
    {
        // Berserker libéré
        card_id: 'b2ad9735-5717-4aaf-aed5-ac033f874874',
        oracle_id: '32e7befb-c083-4e17-aaeb-7247f317bbcf',
        count: 17,
    },
    {
        // Masque d'immolation
        card_id: '0f8b3c24-64e1-44ff-adc7-80825f9f911a',
        oracle_id: 'dd86d11f-0bd8-49a9-936d-e407ea2c1818',
        count: 12,
    },
    {
        // Autel sanguinolent
        card_id: '07c1add6-a0b6-454b-b7d6-eb4f815ec0b8',
        oracle_id: '67631753-f37a-4f41-a6e6-1edead385acc',
        count: 10,
    },
    {
        // Guetteur palustre de Yarok
        card_id: '23d0228f-35b0-4af3-99e4-de82a96ebfba',
        oracle_id: '6bb60f1f-9295-41ce-bdcd-3fe67f8e175c',
        count: 6,
    },
    {
        // Fossoyeur
        card_id: '5fe0ba83-b79b-47d8-af41-8fa9cba80910',
        oracle_id: '1a2030cc-d7ee-4059-b2d7-fb95ea8e267b',
        count: 4,
    },
    {
        // Chef de guerre vengeur
        card_id: 'b57f0b24-6864-4dda-8cab-a3b387ace290',
        oracle_id: 'be3dd4f6-794d-4bce-b71e-4b3477d5d388',
        count: 7,
    },
    {
        // Du sang pour les os
        card_id: '576177d3-2426-458b-8918-93e930ffb66f',
        oracle_id: 'fc7439ec-32e3-47db-9ff4-6eb84cb33d89',
        count: 5,
    },
    {
        // Vampire de la lune sinistre
        card_id: '2ac540db-013e-4aaa-a62e-791ab3b2a1f0',
        oracle_id: '5a938371-8428-48e3-85ed-6a84a39b29c6',
        count: 6,
    },
    {
        // Horrible flagellateur
        card_id: '94d85be5-5bee-4e0b-a0e7-3ce952ee8ea0',
        oracle_id: '1b2993fa-b212-4eaa-b3e8-f79c2f2e4cbe',
        count: 5,
    },
    {
        // Distorsion des pensées
        card_id: '66cad4de-30cf-4fee-ad42-e3d74dff9bff',
        oracle_id: '5f089ac6-9e92-4ec2-bf46-a0b08d1e2979',
        count: 3,
    },
    {
        // Voltigeuse assoiffée de sang
        card_id: '66bed65a-bbb1-4a3c-b7a9-330e30bbec95',
        oracle_id: 'ae78b6f9-8334-479e-8111-8e3acb2b7ca7',
        count: 3,
    },
];

module.exports = {
    card_list_items,
    user_id,
    state,
};
