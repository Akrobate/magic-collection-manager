'use strict';

const user_seed_1 = {
    id: 100,
    password: 'ShouldHaveLettersDigitsAndAtLeast8chars',
    email: 'user_seed_1.user_seed_1@test.com',
};

const user_seed_2 = {
    id: 200,
    password: 'ShouldHaveLettersDigitsAndAtLeast8chars',
    email: 'user_seed_2.user_seed_2@test.com',
};

const user_seed_3 = {
    id: 300,
    password: 'ShouldHaveLettersDigitsAndAtLeast8chars',
    email: 'user_seed_2.user_seed_2@test.com',
};


const collection_user_card_list_seed_1 = {
    id: 1,
    user_id: user_seed_1.id,
    name: null,
    type_id: 1,
};

const collection_user_card_list_seed_2 = {
    id: 2,
    user_id: user_seed_2.id,
    name: null,
    type_id: 1,
};

const collection_user_card_list_seed_3 = {
    id: 3,
    user_id: user_seed_3.id,
    name: null,
    type_id: 1,
};


const collection_user_card_seed_1 = {
    card_list_id: collection_user_card_list_seed_1.id,
    oracle_id: 'ORACLE_ID_SEED_1',
    card_id: 'CARD_ID_SEED_1',
    state: 1,
};

const collection_user_card_seed_2 = {
    card_list_id: collection_user_card_list_seed_1.id,
    oracle_id: 'ORACLE_ID_SEED_2',
    card_id: 'CARD_ID_SEED_2',
    state: 2,
};

const collection_user_card_seed_3 = {
    card_list_id: collection_user_card_list_seed_1.id,
    oracle_id: 'ORACLE_ID_SEED_3',
    card_id: 'CARD_ID_SEED_3',
    state: 3,
};

const collection_user_card_seed_4 = {
    id: 900,
    card_list_id: collection_user_card_list_seed_2.id,
    oracle_id: 'ORACLE_ID_SEED_4_SHOULD_NOT_BE_REMOVED',
    card_id: 'CARD_ID_SEED_4_SHOULD_NOT_BE_REMOVED',
    state: 3,
};

const collection_user_card_seed_5 = {
    id: 1000,
    card_list_id: collection_user_card_list_seed_2.id,
    oracle_id: 'ORACLE_ID_SEED_5',
    card_id: 'CARD_ID_SEED_5',
    state: 3,
};


// Araignée sentinelle - fr - set cmr
const collection_user_card_seed_6 = {
    card_list_id: collection_user_card_list_seed_3.id,
    oracle_id: 'a37c9b13-dc45-4b55-a63d-5314abde7aab',
    card_id: '41e0f8ba-75ab-48ef-8680-7977563b8205',
    state: 1,
};

// Araignée sentinelle - fr - set m13
const collection_user_card_seed_7 = {
    card_list_id: collection_user_card_list_seed_3.id,
    oracle_id: 'a37c9b13-dc45-4b55-a63d-5314abde7aab',
    card_id: '6c0c074d-2e07-4bc9-97c6-250f972c44c0',
    state: 1,
};

// Croissance titanesque - fr - set m19
const collection_user_card_seed_8 = {
    card_list_id: collection_user_card_list_seed_3.id,
    oracle_id: '61e09dd9-7870-48c2-9177-d6abc3162692',
    card_id: '0e6194a6-a2af-42c3-a52f-b2286f61b61d',
    state: 3,
};

// Croissance titanesque - fr
const collection_user_card_seed_9 = {
    card_list_id: collection_user_card_list_seed_3.id,
    oracle_id: '61e09dd9-7870-48c2-9177-d6abc3162692',
    card_id: '0e6194a6-a2af-42c3-a52f-b2286f61b61d',
    state: 2,
};

// Indrik affectueux - fr
const collection_user_card_seed_10 = {
    card_list_id: collection_user_card_list_seed_3.id,
    oracle_id: '88aa032d-dc11-4cae-81f3-ce66353963e0',
    card_id: '49557e4c-4aa7-4d01-8d05-ec83b56587cd',
    state: 2,
};


module.exports = {
    user_seed_1,
    user_seed_2,
    user_seed_3,
    collection_user_card_list_seed_1,
    collection_user_card_list_seed_2,
    collection_user_card_list_seed_3,
    collection_user_card_seed_1,
    collection_user_card_seed_2,
    collection_user_card_seed_3,
    collection_user_card_seed_4,
    collection_user_card_seed_5,
    collection_user_card_seed_6,
    collection_user_card_seed_7,
    collection_user_card_seed_8,
    collection_user_card_seed_9,
    collection_user_card_seed_10,
};
