# Card collection manager

https://api.cardmarket.com/ws/documentation/API_2.0:Market_Place_Information

## Initialization scripts

### DownloadCardData

Download the latest csv file of updated cards. Need to be done each time a new card sets are added.

```sh
node src/scripts/DownloadCardData.js
```