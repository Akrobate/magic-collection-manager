# magic-collection-manager

## Configure dev env to run tests

1. Start databases

```bash
docker-compose -f docker-compose-backends.yml up -d
```

2. Configuration file for tests

Create file configuration.test.yml in backend copiyng the configuration.default.yml

```bash
cp configuration.default.yml configuration.test.yml
```

3. Run tests

```bash
cd ./backend
npm test
npm run cover
npm run lint
```
