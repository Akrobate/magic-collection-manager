'use strict';

import UserLoginFormElement from './UserLoginFormElement'
import UserRegistrationFormElement from './UserRegistrationFormElement'

export {
    UserLoginFormElement,
    UserRegistrationFormElement,
}
