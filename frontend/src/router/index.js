import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'

Vue.use(VueRouter)

const routes = [
    {
        path: '/access',
        name: 'access',
        component: () => import('@/components/pages/AccessPage.vue'),
        meta: {
            requiresAuth: false,
        }
    },
    {
        path: '/about',
        name: 'about',
        component: () => import('@/components/pages/AboutPage.vue'),
        meta: {
            requiresAuth: false,
        }
    },
    {
        path: '/',
        name: 'home',
        component: () => import('@/components/pages/HomePage.vue'),
        meta: {
            requiresAuth: false,
        }
    },
    {
        path: '/collection',
        name: 'collection',
        component: () => import('@/components/pages/MyCollectionPage.vue'),
        meta: {
            requiresAuth: true,
        }
    },
    {
        path: '/collection-info',
        name: 'collection_info',
        component: () => import('@/components/pages/MyCollectionInfoPage.vue'),
        meta: {
            requiresAuth: true,
        }
    },
    {
        path: '/collection-set/:set_id',
        name: 'collection_set',
        component: () => import('@/components/pages/MyCollectionSetPage.vue'),
        meta: {
            requiresAuth: true,
        },
        props: true,
    },
    {
        path: '/decks',
        name: 'decks',
        component: () => import('@/components/pages/DeckManagerPage.vue'),
        meta: {
            requiresAuth: true,
        }
    },
    {
        path: '/decks/:id/statistics',
        name: 'decks_statistics',
        component: () => import('@/components/pages/DeckStatisticsPage.vue'),
        meta: {
            requiresAuth: true,
        },
        props: (route) => ({
            ...route.params,
            card_list_id: parseInt(route.params.id),
        })
    },
    {
        path: '/better-card/:card_id',
        name: 'better_card_finder',
        component: () => import('@/components/pages/BetterCardFinderPage.vue'),
        meta: {
            requiresAuth: true,
        },
        props: true,
    },
    {
        path: '/decks/compare',
        name: 'decks_compare',
        component: () => import('@/components/pages/DeckStatisticsPage.vue'),
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/decks/:id',
        name: 'decks_view',
        component: () => import('@/components/pages/DeckViewCardsPage.vue'),
        meta: {
            requiresAuth: true,
        },
        props: (route) => ({
            ...route.params,
            card_list_id: parseInt(route.params.id),
        })
    },
    {
        path: '/',
        name: 'user-home',
        component: () => import('@/components/pages/UserHomePage.vue'),
        meta: {
            public: false,
        }
    },

]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})


router.beforeEach((to, from, next) => {

    if(
        to.matched.some(record => record.meta.requiresAuth)
        && !store.getters['authentication_store/isConnected']
    ) {
        return next({
                name: 'access',
                params: { nextUrl: to.fullPath }
            })
    }
    return next();
})

export default router
