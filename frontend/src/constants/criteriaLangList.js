'use strict'

const CRITERIA_LANG_LIST = [
    {
        text: 'Anglais',
        value: 'en',
    },
    {
        text: 'Espagnol',
        value: 'es',
    },
    {
        text: 'Francais',
        value: 'fr',
    },
    {
        text: 'Allemand',
        value: 'de',
    },
    {
        text: 'Italien',
        value: 'it',
    },
    {
        text: 'Polonais',
        value: 'pt',
    },
    {
        text: 'Japonais',
        value: 'ja',
    },
    {
        text: 'Coréen',
        value: 'ko',
    },
    {
        text: 'Russe',
        value: 'ru',
    },
    {
        text: 'Chinois simplifié',
        value: 'zhs',
    },
    {
        text: 'Chinois traditionel',
        value: 'zht',
    },
];

export {
    CRITERIA_LANG_LIST,
}
