'use strict'

const SEARCH_ENGINE = {
    MODE: {
        GLOBAL_SEARCH: 1,
        COLLECTION: 2,
        DECK: 3,
    },
}

export {
    SEARCH_ENGINE,
}
