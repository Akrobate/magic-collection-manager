'use strict'


const CRITERIA_KEYWORD_LIST = [
    {
        text: 'Piétinement',
        value: 'Trample',
    },
    {
        text: 'Carrence',
        value: 'Devoid',
    },
    {
        text: 'Eveil',
        value: 'Awaken',
    },
    {
        text: 'Portée',
        value: 'Reach',
    },
    {
        text: 'Convergeance',
        value: 'Converge',
    },
    {
        text: 'Ralliement',
        value: 'Rally',
    },
    {
        text: 'Défense talismanique',
        value: 'Hexproof',
    },
    {
        text: 'Rage',
        value: 'Enrage',
    },
    {
        text: 'Combat',
        value: 'Fight',
    },
    {
        text: 'Exploration',
        value: 'Explore',
    },
    {
        text: 'Défenseur',
        value: 'Defender',
    },
    {
        text: 'Renforcement',
        value: 'Bolster',
    },
    {
        text: 'Vol',
        value: 'Flying',
    },
    {
        text: 'Férocité',
        value: 'Ferocious',
    },
    {
        text: 'Maquis',
        value: 'Undergrowth',
    },
    {
        text: 'Convocation',
        value: 'Convoke',
    },
    {
        text: 'Enchanter',
        value: 'Enchant',
    },
    {
        text: 'Evolution',
        value: 'Evolve',
    },
    {
        text: 'Protection',
        value: 'Protection',
    },
    {
        text: 'Flash',
        value: 'Flash',
    },
    {
        text: 'Célérité',
        value: 'Haste',
    },
    {
        text: 'Recyclage',
        value: 'Cycling',
    },
    {
        text: 'Révolte',
        value: 'Revolt',
    },
    {
        text: 'Coup de sang',
        value: 'Bloodrush',
    },
    {
        text: 'Délire',
        value: 'Delirium',
    },
    {
        text: 'Vigilance',
        value: 'Vigilance',
    },
    {
        text: 'Enquête',
        value: 'Investigate',
    },
    {
        text: 'Sacrifice',
        value: 'Scry',
    },
    {
        text: 'Contact mortel',
        value: 'Deathtouch',
    },
];

export {
    CRITERIA_KEYWORD_LIST,
}
