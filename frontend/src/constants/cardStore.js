'use strict'

const CARD_STORE = {
    SEARCH_MODE: {
        PUBLIC: 1,
        AGGREGATED: 2,
    },
}

export {
    CARD_STORE,
}
