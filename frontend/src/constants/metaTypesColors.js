'use strict'

const META_TYPES_COLORS = [
    {
        value: 'creature',
        color: 'rgb(250, 196, 62)', // yellow #FAC43E
    },
    {
        value: 'planeswalker',
        color: 'rgb(214, 125, 65)', // orange #D67D41
    },
    {
        value: 'sorcery',
        color: 'rgb(123, 86, 245)', // blue #7B56F5
    },
    {
        value: 'enchantment',
        color: 'rgb(235, 65, 59)', // red  #EB413B
    },
    {
        value: 'instant',
        color: 'rgb(75, 124, 250)', // blue tuquoise #4B7CFA
    },
    {
        value: 'artifact',
        color: 'rgb(75, 187, 214)', // blue ciel #4BBBD6
    },
    {
        value: 'land',
        color: 'rgb(53, 212, 67)', // green #35D443
    },
    {
        value: 'phenomenon',
        color: 'rgb(70, 235, 176)', // green flash #46EBB0
    },
    {
        value: 'conspiracy',
        color: 'rgb(169, 245, 61)', // green flash #A9F53D
    },
    {
        value: 'plane',
        color: 'rgb(12, 12, 12)', // black
    },
]

export {
    META_TYPES_COLORS,
}