import Vue from 'vue'
import '@/filters/GlobalFilters'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify';
import Axios from 'axios'
import Vuex from 'vuex'
import { responseSuccess, responseError, requestAuthenticate } from '@/repositories/ApiInterceptors'
import { AppLayout, FullPageLoader, PublicAppLayout } from './components/layouts';

Vue.component('AppLayout', AppLayout)
Vue.component('PublicAppLayout', PublicAppLayout)
Vue.component('FullPageLoader', FullPageLoader)

import store from '@/store'

Vue.prototype.$http = Axios;

Vue.prototype.$http.interceptors.response.use(responseSuccess, responseError)
Vue.prototype.$http.interceptors.request.use(requestAuthenticate)
Vue.config.productionTip = false

Vue.use(Vuex)

new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
}).$mount('#app')
