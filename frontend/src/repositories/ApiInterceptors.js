import router from '@/router'
import store from '@/store'
import moment from 'moment'
// @todo: transformm http magic numbers to constants
// import status_code from '@/constants/httpStatusCodes'

const responseSuccess = function(response) {
    return response
}

const responseError = function(error) {
    console.log(error);
    if (error.response.status === 401) {
        router.push({ name: 'access' })
    }
    return Promise.reject(error)
}


const requestAuthenticate = async (config) => {
    const token = store.getters['authentication_store/token']

    const is_connected = store.getters['authentication_store/isConnected']
    const token_data = store.getters['authentication_store/tokenData']
    
     // @todo: This mecanic could move to the store
    const is_renewing_token = store.getters['authentication_store/isRenewingToken']
    if (
        is_connected
        && (token_data.exp - moment().unix()) < (10 * 60)
        && is_renewing_token === false
        && token_data.exp - moment().unix() < token_data.exp
    ) {
        console.log("Refreshing token", (token_data.exp - moment().unix()) / 60);
        await store.dispatch('authentication_store/renewToken')
    }

    if (is_connected) {
        config.headers = {
            Authorization: `Bearer ${token}`
        }
    }
    return config
};

export {
    responseSuccess,
    responseError,
    requestAuthenticate,
}