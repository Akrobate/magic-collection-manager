import axios from 'axios'
import configuration from '@/configurations/api'

class CardListRepository {
   
    /**
     * @returns {CampaignRepository}
     */
    constructor() {
        this.ressource_url = `${configuration.url_api}/api/v1/users/:user_id/card-lists`
        this.ressource_url_card_list_items = `${configuration.url_api}/api/v1/users/:user_id/card-list-items`
    }
    

    /**
     * @param {Object} criteria 
     * @returns {Promise} 
     */
    async search(user_id, criteria = {}) {
        const result = await axios.get(`${this.ressource_url.replace(':user_id', user_id)}`, {
            params: criteria,
        })

        return result.data
    }

    /**
     * @param {Object} criteria 
     * @returns {Promise} 
     */
    async create({
        user_id,
        type_id,
        name,
    }) {
        const result = await axios.post(
            `${this.ressource_url.replace(':user_id', user_id)}`,
            {
                type_id,
                name,
            }
        )

        return result.data
    }


    /**
     * @param {Object} criteria 
     * @returns {Promise} 
     */
    async duplicate({
        user_id,
        type_id,
        name,
        card_list_id,
    }) {
        const result = await axios.post(
            `${this.ressource_url.replace(':user_id', user_id)}/${card_list_id}/duplicate`,
            {
                type_id,
                name,
            }
        )

        return result.data
    }

    /**
     * @param {Object} criteria 
     * @returns {Promise} 
     */
    async update({
        id,
        user_id,
        type_id,
        name,
    }) {
        const result = await axios.patch(
            `${this.ressource_url.replace(':user_id', user_id)}/${id}`,
            {
                type_id,
                name,
            }
        )

        return result.data
    }

    
    /**
     * /users/:user_id/card-lists/:card_list_id
     * @param {Object} param
     * @returns {Promise}
     */
     async delete({
        user_id,
        card_list_id,
    }) {
        const result = await axios.delete(
            `${this.ressource_url.replace(':user_id', user_id)}/${card_list_id}`,
        )
        return result.data
    }


    /**
     * /users/:user_id/card-lists/:card_list_id/items
     * @param {Object} param
     * @returns {Promise}
     */
    async createCardItem({
        user_id,
        card_list_id,
        oracle_id,
        card_id,
        state,
    }) {
        const result = await axios.post(
            `${this.ressource_url.replace(':user_id', user_id)}/${card_list_id}/items`,
            {
                oracle_id,
                card_id,
                state,
            }
        )
        return result.data
    }


    /**
     * /users/:user_id/card-lists/:card_list_id/items/:card_list_item_id
     * @param {Object} param
     * @returns {Promise}
     */
    async deleteCardListItem({
        user_id,
        card_list_id,
        card_list_item_id
    }) {
        const result = await axios.delete(
            `${this.ressource_url.replace(':user_id', user_id)}/${card_list_id}/items/${card_list_item_id}`,
        )
        return result.data
    }


    /**
     * /users/:user_id/card-lists/:card_list_id/items/:card_list_item_id
     * @param {Object} param
     * @returns {Promise}
     */
    async searchCardListItem({
        user_id,
        card_list_id,
        card_list_id_list,
        card_id,
        card_list_type_id,
    }) {
        const result = await axios.get(
            `${this.ressource_url_card_list_items.replace(':user_id', user_id)}`,
            {
                params: {
                    card_list_id,
                    card_list_id_list,
                    card_id,
                    card_list_type_id,
                }
            }
        )
        return result.data
    }

}

const card_list_repository = new CardListRepository()

export {
    card_list_repository,
}
