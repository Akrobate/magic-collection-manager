import axios from 'axios'
import configuration from '@/configurations/api'

class CardRepository {
   
    /**
     * @returns {CampaignRepository}
     */
    constructor() {
        this.ressource_url = `${configuration.url_api}/api/v1`
    }
    

    /**
     * @param {Object} criteria 
     * @returns {Promise} 
     */
    async search(criteria) {
        const result = await axios.get(`${this.ressource_url}/cards`, {
            params: criteria,
        });

        return result.data
    }


    /**
     * @param {Object} criteria 
     * @returns {Promise} 
     */
    async count(criteria) {
        const result = await axios.get(`${this.ressource_url}/cards/count`, {
            params: criteria,
        });

        return result.data
    }


    /**
     * @param {Object} criteria 
     * @returns {Promise} 
     */
     async aggregatedSearch(user_id, criteria) {
        const result = await axios.get(
            `${this.ressource_url}/users/:user_id/aggregated`.replace(':user_id', user_id),
            {
                params: criteria,
            }
        );
        return result.data
    }


    /**
     * @param {Object} criteria 
     * @returns {Promise} 
     */
    async aggregatedCount(user_id, criteria) {
        const result = await axios.get(
            `${this.ressource_url}/users/:user_id/aggregated/count`.replace(':user_id', user_id),
            {
                params: criteria,
            }
        );
        return result.data
    }


}

const card_repository = new CardRepository()

export {
    card_repository,
}
