
import axios from 'axios'
import configuration from '@/configurations/api'

class CardReferentialRepository {
   
    /**
     * @returns {CampaignRepository}
     */
    constructor() {
        this.ressource_url = `${configuration.url_api}/api/v1/cards/referentials`
    }
    

    /**
     * @param {Object} criteria 
     * @returns {Promise} 
     */
    async getCardReferentialSet() {
        const result = await axios.get(`${this.ressource_url}/sets`)
        return result.data
    }


    /**
     * @param {Object} criteria 
     * @returns {Promise} 
     */
    async getCardReferentialColor() {
        const result = await axios.get(`${this.ressource_url}/colors`)
        return result.data
    }


    /**
     * @param {Object} criteria 
     * @returns {Promise} 
     */
    async getCardReferentialMetaType() {
        const result = await axios.get(`${this.ressource_url}/meta-types`)
        return result.data
    }

}

const card_referential_repository = new CardReferentialRepository()

export {
    card_referential_repository,
}
