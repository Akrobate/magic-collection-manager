import {
    card_repository,
} from '@/repositories'

const state = () => ({
})

const getters = {
}

const actions = {
    async getCards({ rootGetters }, { card_list_id }) {
        const user_id = rootGetters['authentication_store/user_id']
        const cards_count_object = await card_repository.aggregatedCount(user_id, {
            card_list_id,
        })
        const all_cards_object = await card_repository.aggregatedSearch(user_id, {
            card_list_id,
            limit: cards_count_object.card_count,
        })
        return all_cards_object.card_list
    },
}


const mutations = {
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
  