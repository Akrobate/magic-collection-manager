'use strict'

import {
    card_list_repository,
} from '@/repositories'

const state = () => ({
})
  
const getters = {
}

const actions = {
    async getCollectionCardListItemListCByCardId({ rootGetters }, card_id) {
        const card_list = await card_list_repository.searchCardListItem({
            card_id,
            user_id: rootGetters['authentication_store/user_id'],
            card_list_id: rootGetters['card_list_store/getCollectionCardListId'],
        })
        return card_list.card_list_item_list
    }
}

const mutations = {
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
  