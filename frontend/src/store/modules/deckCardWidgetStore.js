'use strict'

import {
    card_list_repository,
} from '@/repositories'

const state = () => ({
    deck_list: [],
})
  
const getters = {
    deckList: (_state) => _state.deck_list,
}

const actions = {

    // @todo should probably move to the cardListStore
    async searchCardListItem({ rootGetters }, input) {
        const card_list = await card_list_repository.searchCardListItem({
            user_id: rootGetters['authentication_store/user_id'],
            ...input,
        })
        return card_list.card_list_item_list
    },

    async init({ rootGetters, commit }) {
        const card_list_list = rootGetters['card_list_store/getDeckList'];
        commit('set_deck_list', card_list_list)
        return null
    },

}

const mutations = {
    set_deck_list(_state, deck_list) {
        _state.deck_list = deck_list
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
  