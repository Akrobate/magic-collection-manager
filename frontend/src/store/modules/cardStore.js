import {
    card_repository,
} from '@/repositories'

import {
    CARD_STORE,
} from '@/constants'

const state = () => ({
    card_list: [],
    card_search_total_count: 0,

    criteria_id_list: null,
    criteria_quick_search: null,
    criteria_collector_number: null,
    criteria_lang: null,
    criteria_color_identity_list: null,
    criteria_set: null,
    criteria_type_list: null,
    criteria_keyword_list: null,
    criteria_card_list_id: null,

    search_mode: CARD_STORE.SEARCH_MODE.PUBLIC,
    limit: 16,
    offset: 0,
    total_pages: null,
    page: 1,

    group_by_oracle_id: null,
    group_by_card_id: null,

    sort: null,
})

const getters = {

    cardList: (_state) => _state.card_list,
    cardSearchTotalCount: (_state) => _state.card_search_total_count,

    criteriaQuickSearch: (_state) => _state.criteria_quick_search,
    criteriaLang: (_state) => _state.criteria_lang,
    criteriaCollectionNumber: (_state) => _state.criteria_collector_number,
    
    criteriaIdList: (_state) => _state.criteria_id_list,
    criteriaSet: (_state) => _state.criteria_set,
    criteriaSetList: (_state) => _state.criteria_set_list,
    criteriaTypeList: (_state) => _state.criteria_type_list,
    criteriaKeywordList: (_state) => _state.criteria_keyword_list,
    criteriaColorIdentityList: (_state) => _state.criteria_color_identity_list,
    criteriaCardListId: (_state) => _state.criteria_card_list_id,
    
    limit: (_state) => _state.limit,
    page: (_state) => _state.page,
    totalPages: (_state) => _state.total_pages,
    sort: (_state) => _state.sort,
    groupByOracleId: (_state) => _state.group_by_oracle_id,
    groupByCardId: (_state) => _state.group_by_card_id,

}

const actions = {

    getCriteria({ _state }) {
        return {
            limit: _state.limit,
            offset: (_state.page - 1) * _state.limit,
            quick_search: _state.criteria_quick_search === null ? undefined : _state.criteria_quick_search,
            lang: _state.criteria_lang === null ? undefined : _state.criteria_lang,
            collector_number: _state.criteria_collector_number === null ? undefined : _state.criteria_collector_number,
            id_list: _state.criteria_id_list === null ? undefined : _state.criteria_id_list,
            set: _state.criteria_set === null ? undefined : _state.criteria_set,
            set_list: _state.criteria_set_list === null ? undefined : _state.criteria_set_list,
            type_list: _state.criteria_type_list === null ? undefined : _state.criteria_type_list,
            keyword_list: _state.criteria_keyword_list === null ? undefined : _state.criteria_keyword_list,
            color_identity_list: _state.criteria_color_identity_list === null ? undefined : _state.criteria_color_identity_list,
            card_list_id: _state.criteria_card_list_id === null ? undefined : _state.criteria_card_list_id,
            group_by_card_id: _state.group_by_card_id === null ? undefined : _state.group_by_card_id,
            group_by_oracle_id: _state.group_by_oracle_id === null ? undefined : _state.group_by_oracle_id,
            sort: _state.sort === null ? undefined : _state.sort,
        }
    },

    async processSearchAndCount({ dispatch }) {
        await dispatch('app_layout_store/setLoading', true, { root: true } )
        await dispatch('processSearchData')
        await dispatch('processSearchCount')
        await dispatch('app_layout_store/setLoading', false, { root: true } )
    },

    async processSearchData({ dispatch }) {
        const criteria = await dispatch('getCriteria')
        await dispatch('search', criteria)
    },

    async processSearchCount({ dispatch }) {
        const criteria = await dispatch('getCriteria')
        await dispatch('count', {
            ...criteria,
            limit: 0,
            offset: 0,
        })
    },

    async search({ commit, state: _state, rootState }, criteria) {
        let results = []

        if (_state.search_mode  === CARD_STORE.SEARCH_MODE.PUBLIC) {
            results = await card_repository.search(criteria)
        } else {
            results = await card_repository.aggregatedSearch(
                rootState.authentication_store.token_data.user_id,
                criteria
            )
        }

        commit('set_card_list', results.card_list)
    },

    async count({ commit, state: _state, rootState }, criteria) {
        let results = null
        if (_state.search_mode  === CARD_STORE.SEARCH_MODE.PUBLIC) {
            results = await card_repository.count({
                ...criteria,
                limit: 0,
            })
        } else {
            results = await card_repository.aggregatedCount(
                rootState.authentication_store.token_data.user_id,
                criteria
            )
        }
        
        commit('set_card_search_total_count', results.card_count)
    },

    async reset({ commit }) {

        commit('set_card_list', [])
        commit('set_card_search_total_count', 0)

        commit('set_criteria_quick_search', null)
        commit('set_criteria_lang', null)
        commit('set_criteria_collector_number', null)
        commit('set_criteria_color_identity_list', null)
        commit('set_criteria_set', null)
        commit('set_criteria_set_list', null)
        commit('set_criteria_type_list', null)
        commit('set_criteria_keyword_list', null)
        commit('set_criteria_card_list_id', null)
        commit('set_criteria_id_list', null)
        
        commit('set_search_mode', CARD_STORE.SEARCH_MODE.PUBLIC)
        commit('set_limit', 16)
        commit('set_offset', 0)
        commit('set_total_pages', null)
        commit('set_page', 1)

        commit('set_group_by_oracle_id', null)
        commit('set_group_by_card_id', null)

        commit('set_sort', null)
    },

    prepareData(_, data) {
        if (data.card_faces) {
            return {
                ...data,
                ...data.card_faces[0]
            }
        }
        return data
    },
}

const mutations = {

    set_card_list(_state, card_list) {
        _state.card_list = card_list
    },
    set_card_search_total_count(_state, card_search_total_count) {
        _state.card_search_total_count = card_search_total_count
    },


    set_criteria_id_list(_state, criteria_id_list) {
        _state.criteria_id_list = criteria_id_list
    },
    set_criteria_quick_search(_state, criteria_quick_search) {
        _state.criteria_quick_search = criteria_quick_search
    },
    set_criteria_lang(_state, criteria_lang) {
        _state.criteria_lang = criteria_lang
    },
    set_criteria_collector_number(_state, criteria_collector_number) {
        _state.criteria_collector_number = criteria_collector_number
    },
    set_criteria_color_identity_list(_state, criteria_color_identity_list) {
        _state.criteria_color_identity_list = criteria_color_identity_list
    },
    set_criteria_set(_state, criteria_set) {
        _state.criteria_set = criteria_set
    },
    set_criteria_set_list(_state, criteria_set_list) {
        _state.criteria_set_list = criteria_set_list
    },
    set_criteria_type_list(_state, criteria_type_list) {
        _state.criteria_type_list = criteria_type_list
    },
    set_criteria_keyword_list(_state, criteria_keyword_list) {
        _state.criteria_keyword_list = criteria_keyword_list
    },
    set_criteria_card_list_id(_state, criteria_card_list_id) {
        _state.criteria_card_list_id = criteria_card_list_id
    },


    set_search_mode(_state, search_mode) {
        _state.search_mode = search_mode
    },
    set_limit(_state, limit) {
        _state.limit = limit
    },
    set_offset(_state, offset) {
        _state.offset = offset
    },
    set_total_pages(_state, total_pages) {
        _state.total_pages = total_pages
    },
    set_page(_state, page) {
        _state.page = page
    },

    set_group_by_oracle_id(_state, group_by_oracle_id) {
        _state.group_by_oracle_id = group_by_oracle_id
    },
    set_group_by_card_id(_state, group_by_card_id) {
        _state.group_by_card_id = group_by_card_id
    },

    set_sort(_state, sort) {
        _state.sort = sort
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
  