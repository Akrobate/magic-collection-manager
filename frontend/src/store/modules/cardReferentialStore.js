'use strict'

import {
    card_referential_repository,
} from '@/repositories'

const state = () => ({
    card_referential_set_list: [],
    card_referential_color_list: [],
    card_referential_meta_type_list: [],
})
  
const getters = {
    getCardReferentialSetList: (_state) => _state.card_referential_set_list,
    getCardReferentialColorList: (_state) => _state.card_referential_color_list,
    getCardReferentialMetaTypeList: (_state) => _state.card_referential_meta_type_list,
}

const actions = {

    async getCardReferentialSet() {
        const result = await card_referential_repository.getCardReferentialSet()
        return result.set_list
    },

    async getCardReferentialColor() {
        const result = await card_referential_repository.getCardReferentialColor()
        return result.color_list
    },

    async getCardReferentialMetaType() {
        const result = await card_referential_repository.getCardReferentialMetaType()
        return result.meta_type_list
    },

    async init({ dispatch }) {
        await dispatch('initSet') 
        await dispatch('initColor') 
        await dispatch('initMetaType') 
    },

    async initSet({ dispatch, commit }) {
        const card_referential_set_list = await dispatch('getCardReferentialSet');
        commit('set_card_referential_set_list', card_referential_set_list)
    },

    async initColor({ dispatch, commit }) {
        const card_referential_color_list = await dispatch('getCardReferentialColor');
        commit('set_card_referential_color_list', card_referential_color_list)
    },

    async initMetaType({ dispatch, commit }) {
        const card_referential_meta_type_list = await dispatch('getCardReferentialMetaType');
        commit('set_card_referential_meta_type_list', card_referential_meta_type_list)
    },

}

const mutations = {
    set_card_referential_set_list(_state, card_referential_set_list) {
        _state.card_referential_set_list = card_referential_set_list
    },
    set_card_referential_color_list(_state, card_referential_color_list) {
        _state.card_referential_color_list = card_referential_color_list
    },
    set_card_referential_meta_type_list(_state, card_referential_meta_type_list) {
        console.log("card_referential_meta_type_list", card_referential_meta_type_list)
        _state.card_referential_meta_type_list = card_referential_meta_type_list
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
  