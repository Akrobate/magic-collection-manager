import {
    card_list_repository,
} from '@/repositories'
  

const state = () => ({
    card_list_list: [],
})

const getters = {
    cardListList: (_state) => _state.card_list_list,
    getCollectionCardListId: (_state) => _state.card_list_list.find((item) => item.type_id === 1).id,
    getDeckList: (_state) => _state.card_list_list.filter((item) => item.type_id === 2),
}

const actions = {

    async init({ rootGetters, dispatch }) {
        await dispatch('search', {
            user_id: rootGetters['authentication_store/user_id'],
            criteria: {}
        })
    },

    async search({ commit }, {
        user_id,
        criteria = {}
    }) {
        const results = await card_list_repository.search(user_id, criteria)
        commit('set_card_list_list', results.card_list_list)
    },

    getById({ getters: _getters }, id) {
        return _getters.cardListList.find((card_list) => card_list.id === id)
    },

    addCardToCollectionList({ dispatch, getters }, {
        user_id,
        oracle_id,
        card_id,
        state: _state,
    }) {
        const card_list_id = getters.getCollectionCardListId
        return dispatch('addCardToList', {
            user_id,
            card_list_id,
            oracle_id,
            card_id,
            _state,
        })
    },

    addCardToList(_, {
        user_id,
        card_list_id,
        oracle_id,
        card_id,
        state: _state,
    }) {
        return card_list_repository.createCardItem({
            user_id,
            card_list_id,
            oracle_id,
            card_id,
            _state,
        })
    },

    create({ rootGetters }, {
        name,
        type_id,
    }) {
        return card_list_repository.create({
            user_id: rootGetters['authentication_store/user_id'],
            name,
            type_id,
        })
    },

    update({ rootGetters }, {
        id,
        name,
        type_id,
    }) {
        return card_list_repository.update({
            id,
            user_id: rootGetters['authentication_store/user_id'],
            name,
            type_id,
        })
    },

    delete({ rootGetters }, {
        card_list_id,
    }) {
        return card_list_repository.delete({
            user_id: rootGetters['authentication_store/user_id'],
            card_list_id,
        })
    },

    deleteCardListItem(_, {
        user_id,
        card_list_id,
        card_list_item_id,
    }) {
        return card_list_repository.deleteCardListItem({
            user_id,
            card_list_id,
            card_list_item_id,
        })
    },
    
}


const mutations = {
    set_user_id(_state, user_id) {
        _state.user_id = user_id
    },
    set_card_list_list(_state, card_list_list) {
        _state.card_list_list = card_list_list
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
  