import Vue from 'vue'
import Vuex from 'vuex'
import appLayoutStore from './modules/appLayoutStore'
import userStore from './modules/userStore'
import authenticationStore from './modules/authenticationStore'
import cardStore from './modules/cardStore'
import cardListStore from './modules/cardListStore'
import collectionCardWidgetStore from './modules/collectionCardWidgetStore'
import deckCardWidgetStore from './modules/deckCardWidgetStore'
import cardReferentialStore from './modules/cardReferentialStore'
import chartStore from './modules/chartStore'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        app_layout_store: appLayoutStore,
        user_store: userStore,
        authentication_store: authenticationStore,
        card_store: cardStore,
        card_list_store: cardListStore,
        collection_card_widget_store: collectionCardWidgetStore,
        deck_card_widget_store: deckCardWidgetStore,
        card_referential_store: cardReferentialStore,
        chart_store: chartStore,
    },
    actions: {
        async init({ rootGetters, dispatch }) {
            const is_connected = rootGetters['authentication_store/isConnected']
            if (is_connected) {
                await dispatch('authentication_store/init', null, { root: true } )
                await dispatch('card_list_store/init', null, { root: true } )
            }
            await dispatch('card_referential_store/init', null, { root: true } )
        },
    },
    // strict: debug,
    // plugins: debug ? [createLogger()] : []
});
